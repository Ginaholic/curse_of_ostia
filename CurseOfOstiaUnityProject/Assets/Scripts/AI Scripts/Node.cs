﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node : MonoBehaviour
{
    public int gCost;
    public int hCost;
    public int gridX, gridY;
    public bool walkable = true;
    public List<Node> neighbourNodes;
    public Node parent;    

    public int fCost
    {
        get
        {
            return gCost + hCost;
        }
    }
}