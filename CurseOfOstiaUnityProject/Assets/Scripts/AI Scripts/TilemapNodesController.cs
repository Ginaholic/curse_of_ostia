﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class TilemapNodesController : MonoBehaviour
{
    public static TilemapNodesController instance;

    public Tilemap floorTilemap;
    public Tilemap unwalkable;

    public int tilemapAreaLeft = Constants.tilemapAreaLeft;
    public int tilemapAreaBottom = Constants.tilemapAreaBottom;
    public int tilemapAreaRight = Constants.tilemapAreaRight;
    public int tilemapAreaTop = Constants.tilemapAreaTop;

    public int widthOfNodes;
    public int heightOfNodes;

    public GameObject nodePrefab;
    public GameObject nodeCollection;

    public List<GameObject> unsortedNodes;
    public GameObject[,] nodes;

    void Awake()
    {
        MakeInstance();
        CreateNodeFromTilemaps();
    }
    
    /// <summary>
    /// Initialise controller instance
    /// </summary>
    void MakeInstance()
    {
        if (instance == null)
        {
            instance = this;
        }
    }
    
    /// <summary>
    /// Reconstruct nodes array using the unsorted nodes list
    /// </summary>
    void ReconstructNodeArray()
    {
        nodes = new GameObject[widthOfNodes, heightOfNodes];
        
        foreach(GameObject nodeGameObject in unsortedNodes)
        {
            Node node = nodeGameObject.GetComponent<Node>();
            nodes[node.gridX, node.gridY] = nodeGameObject;
        }
    }
    
    /// <summary>
    /// Get all walkable nodes within a patrol area
    /// </summary>
    /// <param name="patrolAreaCenter">Center position of patrol area</param>
    /// <param name="patrolAreaSize">Size of patrol area</param>
    /// <returns>List of all walkable nodes</returns>
    public List<GameObject> GetPatrolAreaNodes(Vector2 patrolAreaCenter, Vector2 patrolAreaSize)
    {
        List<GameObject> patrolNodes = new List<GameObject>();

        float patrolAreaLeft = patrolAreaCenter.x - (patrolAreaSize.x / 2);
        float patrolAreaRight = patrolAreaCenter.x + (patrolAreaSize.x / 2);
        float patrolAreaTop = patrolAreaCenter.y + (patrolAreaSize.y / 2);
        float patrolAreaBottom = patrolAreaCenter.y - (patrolAreaSize.y / 2);

        foreach (GameObject nodeGameObject in unsortedNodes)
        {
            if (nodeGameObject.transform.position.x > patrolAreaLeft && nodeGameObject.transform.position.x < patrolAreaRight && nodeGameObject.transform.position.y > patrolAreaBottom && nodeGameObject.transform.position.y < patrolAreaTop)
            {
                if (nodeGameObject.GetComponent<Node>().walkable == true)
                {
                    patrolNodes.Add(nodeGameObject);
                }
            }
        }
        return patrolNodes;
    }

    /// <summary>
    /// Find the nearest node to a position
    /// </summary>
    /// <param name="position">Position to find nearest node to</param>
    /// <returns>Nearest node</returns>
    public Node FindNearestNode(Vector3 position)
    {
        GameObject nearestNode = null;
        float nearestDistance = 9999999f;
        
        for (int x = 0; x <= nodes.GetUpperBound(0); x++)
        {
            for (int y = 0; y <= nodes.GetUpperBound(1); y++)
            {
                if (nodes[x, y] != null)
                {
                    float distance = Vector2.Distance(nodes[x, y].transform.position, position);
                    if (distance < nearestDistance && nodes[x, y].GetComponent<Node>().walkable == true)
                    {
                        nearestDistance = distance;
                        nearestNode = nodes[x, y];
                    }
                }
            }
        }
        return nearestNode.GetComponent<Node>();
    }
    
    /// <summary>
    /// Generate pathfinding nodes using tilemaps
    /// </summary>
    void CreateNodeFromTilemaps()
    {
        ClearExistingNodes();

        int gridX = 0;
        int gridY = 0;
        int boundX = 0;
        int boundY = 0;

        for (int x = tilemapAreaLeft; x < tilemapAreaRight; x++)
        {
            for (int y = tilemapAreaBottom; y < tilemapAreaTop; y++)
            {
                Vector3Int tileWorldPosition = new Vector3Int(x, y, 0);

                if (floorTilemap.GetTile(floorTilemap.WorldToCell(tileWorldPosition)) != null)
                {
                    Vector3 tileCenterWorldPosition = new Vector3(tileWorldPosition.x + Constants.distanceToNodeCenter, tileWorldPosition.y + Constants.distanceToNodeCenter, tileWorldPosition.z);

                    if (unwalkable.GetTile(unwalkable.WorldToCell(tileWorldPosition)) == true)
                    {
                        GameObject nodeGameObject = Instantiate(nodePrefab, tileCenterWorldPosition, Quaternion.Euler(0, 0, 0));
                        nodeGameObject.GetComponent<SpriteRenderer>().color = Color.red;
                        nodeGameObject.name = "Unwalkable Node " + gridX.ToString() + " : " + gridY.ToString();
                        nodeGameObject.transform.SetParent(nodeCollection.transform);

                        Node node = nodeGameObject.GetComponent<Node>();
                        node.gridX = gridX;
                        node.gridY = gridY;
                        node.walkable = false;
                        unsortedNodes.Add(nodeGameObject);
                    }
                    else
                    {
                        GameObject nodeGameObject = Instantiate(nodePrefab, tileCenterWorldPosition, Quaternion.Euler(0, 0, 0));
                        nodeGameObject.name = "Node " + gridX.ToString() + " : " + gridY.ToString();
                        nodeGameObject.transform.SetParent(nodeCollection.transform);

                        Node node = nodeGameObject.GetComponent<Node>();
                        node.gridX = gridX;
                        node.gridY = gridY;
                        unsortedNodes.Add(nodeGameObject);
                    }
                    gridY++;
                    boundX = gridX;
                    boundY = gridY;
                }
            }
            gridX++;
            gridY = 0;
        }
        
        nodes = new GameObject[boundX + 1, boundY + 1];
        
        foreach (GameObject nodeGameObject in unsortedNodes)
        {
            Node node = nodeGameObject.GetComponent<Node>();
            List<Node> neighbours = new List<Node>();

            foreach (GameObject otherNodeGameObject in unsortedNodes)
            {
                if (otherNodeGameObject != nodeGameObject)
                {
                    if (Vector2.Distance(node.transform.position, otherNodeGameObject.transform.position) <= Constants.distanceBetweenNodes)
                    {
                        neighbours.Add(otherNodeGameObject.GetComponent<Node>());
                    }
                }
            }
            node.neighbourNodes = neighbours;

            nodes[node.gridX, node.gridY] = nodeGameObject;
        }
    }

    /// <summary>
    /// Destroy all node game objects and reset nodes array and list
    /// </summary>
    void ClearExistingNodes()
    {
        foreach(GameObject nodeGameObject in unsortedNodes)
        {
            DestroyImmediate(nodeGameObject);
        }
        
        nodes = new GameObject[0,0];
        unsortedNodes.Clear();
    }
}
