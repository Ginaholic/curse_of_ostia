﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pathfinding : MonoBehaviour
{
    public static Pathfinding instance;
    
    void Awake()
    {
        MakeInstance();
    }

    /// <summary>
    /// Initialise controller instance
    /// </summary>
    void MakeInstance()
    {
        if (instance == null)
        {
            instance = this;
        }
    }
    
    /// <summary>
    /// Find path from start to target
    /// </summary>
    /// <param name="startGameObject">Node gameobject at starting position</param>
    /// <param name="targetGameObject">Node gameobject at the target positions</param>
    /// <returns>List of nodes in the path</returns>
    public List<Node> FindPath(GameObject startGameObject, GameObject targetGameObject)
    {
        Node startNode = TilemapNodesController.instance.FindNearestNode(startGameObject.transform.position);
        Node targetNode = TilemapNodesController.instance.FindNearestNode(targetGameObject.transform.position);

        if (targetNode.walkable == false)
        {
            targetNode = GetNearestWalkableNode(targetNode);
        }
        if (startNode == null || targetNode == null)
        {
            return null;
        }
        
        List<Node> openNodes = new List<Node>();
        HashSet<Node> closedNodes = new HashSet<Node>();

        openNodes.Add(startNode);

        while (openNodes.Count > 0)
        {
            Node currentNode = openNodes[0];

            for (int i = 1; i < openNodes.Count; i++)
            {
                if (openNodes[i].fCost < currentNode.fCost || openNodes[i].fCost == currentNode.fCost)
                {
                    if (openNodes[i].hCost < currentNode.hCost)
                    {
                        currentNode = openNodes[i];
                    }
                }
            }

            openNodes.Remove(currentNode);
            closedNodes.Add(currentNode);

            if (currentNode == targetNode)
            {
                return RetracePath(startNode, targetNode);
            }

            foreach (Node neighbour in currentNode.neighbourNodes)
            {
                if (!neighbour.walkable || closedNodes.Contains(neighbour) || neighbour == null || currentNode == null)
                {
                    continue;
                }

                int newCostToNeighbour = currentNode.gCost + GetHeuristicDistance(currentNode, neighbour);
                if (newCostToNeighbour < neighbour.gCost || !openNodes.Contains(neighbour))
                {
                    neighbour.gCost = newCostToNeighbour;
                    neighbour.hCost = GetHeuristicDistance(neighbour, targetNode);
                    neighbour.parent = currentNode;
                    
                    if (!openNodes.Contains(neighbour))
                    {
                        openNodes.Add(neighbour);
                    }
                }
            }
        }
        return null;
    }

    /// <summary>
    /// Get nearest walkable neighbour node
    /// </summary>
    /// <param name="unwalkableNode">Node to find nearest node to</param>
    /// <returns>Nearest walkable neighbour node or null</returns>
    Node GetNearestWalkableNode(Node unwalkableNode)
    {
        foreach (Node neighbour in unwalkableNode.neighbourNodes)
        {
            if (neighbour.walkable == true)
            {
                return neighbour;
            }
        }
        return null;
    }

    /// <summary>
    /// Retrace path via parent nodes and order path from start to target 
    /// </summary>
    /// <param name="startNode">Path starting node</param>
    /// <param name="targetNode">Path target node</param>
    /// <returns>List of nodes in the path correctly ordered</returns>
    List<Node> RetracePath(Node startNode, Node targetNode)
    {
        List<Node> path = new List<Node>();
        Node currentNode = targetNode;

        while (currentNode != startNode)
        {
            path.Add(currentNode);
            currentNode = currentNode.parent;
        }

        path.Reverse();
        return path;
    }

    /// <summary>
    /// Get heuristic distance between nodes using Manhattan Distance4
    /// </summary>
    /// <param name="nodeA">First node</param>
    /// <param name="nodeB">Second node</param>
    /// <returns>Distance between nodeA and nodeB</returns>
    int GetHeuristicDistance(Node nodeA, Node nodeB)
    {
        int xDistance = Mathf.Abs(nodeA.gridX - nodeB.gridX);
        int yDistance = Mathf.Abs(nodeA.gridY - nodeB.gridY);

        if (xDistance > yDistance)
        {
            return 14 * yDistance + 10 * (xDistance - yDistance);
        }
        return 14 * xDistance + 10 * (yDistance - xDistance);
    }
}
