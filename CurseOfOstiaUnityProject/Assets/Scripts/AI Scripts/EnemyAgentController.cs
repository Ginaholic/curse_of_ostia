﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAgentController : MonoBehaviour
{
    private EnemyController myEnemyController;

    public GameObject playerGameObject;

    public LineRenderer myPatrolLineRenderer;
    public LineRenderer myPathLineRenderer;
    
    public string currentState;

    public Vector2 patrolAreaCenter;
    public Vector2 patrolAreaSize;

    public List<GameObject> myPatrolAreaNodes;
    public List<Node> currentPath;
    public GameObject pathTarget;
    public int pathIndex = 0;
    public float timeSinceLastAttack;

    void Awake()
    {
        myEnemyController = GetComponent<EnemyController>();
    }

    void Start()
    {
        myPatrolAreaNodes = TilemapNodesController.instance.GetPatrolAreaNodes(patrolAreaCenter, patrolAreaSize);
    }

    void Update()
    {
        //Draw debug graphics
        if (DebugGraphicsController.pathsOn)
        {
            if (myPathLineRenderer != null && currentPath.Count > 0)
            {
                myPathLineRenderer.positionCount = currentPath.Count;

                for (int i = 0; i < currentPath.Count; i++)
                {
                    myPathLineRenderer.SetPosition(i, new Vector3(currentPath[i].transform.position.x, currentPath[i].transform.position.y, -1));
                }
            }

            if (myPatrolLineRenderer != null)
            {
                myPatrolLineRenderer.positionCount = 4;

                myPatrolLineRenderer.SetPosition(0, new Vector3(patrolAreaCenter.x - (patrolAreaSize.x / 2), patrolAreaCenter.y + (patrolAreaSize.y / 2), -1));
                myPatrolLineRenderer.SetPosition(1, new Vector3(patrolAreaCenter.x + (patrolAreaSize.x / 2), patrolAreaCenter.y + (patrolAreaSize.y / 2), -1));
                myPatrolLineRenderer.SetPosition(2, new Vector3(patrolAreaCenter.x + (patrolAreaSize.x / 2), patrolAreaCenter.y - (patrolAreaSize.y / 2), -1));
                myPatrolLineRenderer.SetPosition(3, new Vector3(patrolAreaCenter.x - (patrolAreaSize.x / 2), patrolAreaCenter.y - (patrolAreaSize.y / 2), -1));
            }
        }
        else
        {
            myPathLineRenderer.positionCount = 0;
            myPatrolLineRenderer.positionCount = 0;
        }

        timeSinceLastAttack += Time.deltaTime;
        if (!myEnemyController.isDead)
        {
            if (IsPlayerVisible() && IsPlayerInPatrolArea())
            {
                if (IsPlayerInRange(myEnemyController.attackRange))
                {
                    Attack();
                }
                else
                {
                    Chase();
                }
            }
            else
            {
                Patrol();
            }
        }
    }
    
    /// <summary>
    /// Check if player is within FOW 
    /// </summary>
    /// <returns>Bool player in sight</returns>
    bool IsPlayerVisible()
    {
        float angleToPlayer = (Vector3.Angle(playerGameObject.transform.position - transform.position, new Vector3(myEnemyController.myAnimator.GetFloat(Constants.animatorFaceXFloat), myEnemyController.myAnimator.GetFloat(Constants.animatorFaceYFloat), 0)));

        return angleToPlayer >= myEnemyController.visionMinAngle && angleToPlayer <= myEnemyController.visionMaxAngle && IsPlayerInRange(myEnemyController.visionRange);
    }

    /// <summary>
    /// Check if player is inside patrol area
    /// </summary>
    /// <returns>Bool player in patrol area</returns>
    bool IsPlayerInPatrolArea()
    {
        return myPatrolAreaNodes.Contains(TilemapNodesController.instance.FindNearestNode(playerGameObject.transform.position).gameObject);
    }

    /// <summary>
    /// Check if player is within range
    /// </summary>
    /// <param name="range">Range to check in</param>
    /// <returns></returns>
    bool IsPlayerInRange(float range)
    {
        return Vector3.Distance(playerGameObject.transform.position, transform.position) <= range;
    }

    /// <summary>
    /// Patrol along path and get a new path when target reached
    /// </summary>
    void Patrol()
    {
        currentState = Constants.enemyAgentPatrolState;

        if (currentPath.Count <= 0 || Vector2.Distance(transform.position, pathTarget.transform.position) < Constants.distanceToPoint || pathTarget == playerGameObject)
        {
            pathTarget = myPatrolAreaNodes[Random.Range(0, myPatrolAreaNodes.Count)];
            pathIndex = 0;

            currentPath = Pathfinding.instance.FindPath(gameObject, pathTarget);
        }

        else if (Vector2.Distance(transform.position, currentPath[pathIndex].transform.position) < Constants.distanceToPoint)
        {
            pathIndex++;

            if (pathIndex >= currentPath.Count)
            {
                pathTarget = myPatrolAreaNodes[Random.Range(0, myPatrolAreaNodes.Count)];
                pathIndex = 0;

                currentPath = Pathfinding.instance.FindPath(gameObject, pathTarget);
            }
        }

        myEnemyController.Move(currentPath[pathIndex].transform.position - transform.position);
    }
    
    /// <summary>
    /// Get path to the player and follow it
    /// </summary>
    void Chase()
    {
        currentState = Constants.enemyAgentChaseState;
        
        if (pathTarget != playerGameObject || Vector2.Distance(transform.position, pathTarget.transform.position) < Constants.distanceToPoint)
        {
            pathTarget = playerGameObject;
            pathIndex = 0;

            currentPath = Pathfinding.instance.FindPath(gameObject, pathTarget);
        }
        
        else if (Vector2.Distance(transform.position, currentPath[pathIndex].transform.position) < Constants.distanceToPoint)
        {
            pathIndex++;
            
            if (pathIndex >= currentPath.Count)
            {
                pathIndex = 0;

                currentPath = Pathfinding.instance.FindPath(gameObject, pathTarget);
            }
        }
        
        myEnemyController.Move(currentPath[pathIndex].transform.position - transform.position);
    }

    /// <summary>
    /// Attack player if haven't attacked for longer than limit
    /// </summary>
    void Attack()
    {
        if (timeSinceLastAttack >= myEnemyController.attackTimerMin)
        {
            currentState = Constants.enemyAgentAttackState;

            Vector3 directionToPlayer = playerGameObject.transform.position - transform.position;
            if (Mathf.Max(Mathf.Abs(directionToPlayer.x), Mathf.Abs(directionToPlayer.y)) == Mathf.Abs(directionToPlayer.y))
            {
                if (directionToPlayer.y < 0)
                {
                    myEnemyController.Attack(Constants.enemyAttackDown);
                }
                else
                {
                    myEnemyController.Attack(Constants.enemyAttackUp);
                }
            }
            else
            {
                if (directionToPlayer.x < 0)
                {
                    myEnemyController.Attack(Constants.enemyAttackLeft);
                }
                else
                {
                    myEnemyController.Attack(Constants.enemyAttackRight);
                }
            }
            
            timeSinceLastAttack = 0;
        }
    }
}
