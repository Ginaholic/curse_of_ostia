﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicsManager : MonoBehaviour
{
   	void Update()
    {
        foreach (ObjectContainer physicsObjectContainer in PhysicsObjectsContainerHolder.physicsObjectContainers)
        {
            ObjectRigidBody physicsObjectRigidbody = physicsObjectContainer.objectRigidBody;
            
            if (physicsObjectRigidbody.isDynamic)
            {
                physicsObjectRigidbody.ApplyForce(WindController.currentWindForce.x, WindController.currentWindForce.y);
                physicsObjectRigidbody.UpdatePosition();
                
                for (int i = 0; i < PhysicsObjectsContainerHolder.physicsObjectContainers.Count; i++)
                {
                    if (physicsObjectContainer != PhysicsObjectsContainerHolder.physicsObjectContainers[i])
                    {
                        if (physicsObjectContainer.objectShape.CollisionTest(PhysicsObjectsContainerHolder.physicsObjectContainers[i].objectShape))
                        {
                            CollisionResolutionController.ResolveCollision(physicsObjectContainer, PhysicsObjectsContainerHolder.physicsObjectContainers[i]);
                        }
                    }
                }
            }
        }
    }

    //After checking for collisions on all physics objects, update the physics object container list
    void LateUpdate()
    {
        if (PhysicsObjectsContainerHolder.physicsObjectContainersToRemove.Count > 0)
        {
            foreach (ObjectContainer objContainer in PhysicsObjectsContainerHolder.physicsObjectContainersToRemove)
            {
                PhysicsObjectsContainerHolder.physicsObjectContainers.Remove(objContainer);
            }
            
            PhysicsObjectsContainerHolder.physicsObjectContainersToRemove.Clear();
        }
    }
}
