﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PhysicsObjectsContainerHolder
{
    public static List<ObjectContainer> physicsObjectContainers = new List<ObjectContainer>();
    public static List<ObjectContainer> physicsObjectContainersToRemove = new List<ObjectContainer>();
}
