﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectContainer : MonoBehaviour
{
    public static ObjectContainer instance;
    
    public ObjectMaterial objectMaterial;
    public ObjectShape objectShape;
    public ObjectRigidBody objectRigidBody;
    
    void Awake()
    {
        MakeInstance();

        objectMaterial = transform.GetComponent<ObjectMaterial>();
        objectShape = transform.GetComponent<ObjectShape>();
        objectRigidBody = transform.GetComponent<ObjectRigidBody>();
    }

    /// <summary>
    /// Initialise controller instance
    /// </summary>
    void MakeInstance()
    {
        if (instance == null)
        {
            instance = this;
        }
    }
}
