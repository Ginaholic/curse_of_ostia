﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ObjectShape : MonoBehaviour
{        
    public abstract bool CollisionTest(ObjectShape otherShape);
    
    public ObjectContainer myObjectContainer;
    public LineRenderer myLineRenderer;

    //Use an offset from the center to position the collider bounds
    public float centerOffsetX;
    public float centerOffsetY;

    public virtual void Start()
    {
        myObjectContainer = GetComponent<ObjectContainer>();
        PhysicsObjectsContainerHolder.physicsObjectContainers.Add(GetComponent<ObjectContainer>());
    }
}
