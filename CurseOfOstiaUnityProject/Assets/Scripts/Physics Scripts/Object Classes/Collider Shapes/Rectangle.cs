﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rectangle : ObjectShape
{
    public float width;
    public float height;

    public float myRight;
    public float myLeft;
    public float myTop;
    public float myBottom;
    
    public override void Start()
    {
        base.Start();

        //Get initial shape bounds
        UpdateLeftRightTopBottom();
    }

    void Update()
    {
        //If game object is dynamic, recalculate bounds every frame
        if (myObjectContainer.objectRigidBody.isDynamic)
        {
            UpdateLeftRightTopBottom();
        }
        
        //Draw debug graphics
        if (DebugGraphicsController.collidersOn && myLineRenderer != null)
        {
            myLineRenderer.positionCount = 4;

            myLineRenderer.SetPosition(0, new Vector3(myLeft, myTop, -1));
            myLineRenderer.SetPosition(1, new Vector3(myRight, myTop, -1));
            myLineRenderer.SetPosition(2, new Vector3(myRight, myBottom, -1));
            myLineRenderer.SetPosition(3, new Vector3(myLeft, myBottom, -1));
        }
        else
        {
            myLineRenderer.positionCount = 0;
        }
    }
    
    /// <summary>
    /// Calculate the shape's bounds 
    /// </summary>
    public void UpdateLeftRightTopBottom()
    {
        myLeft = (myObjectContainer.objectRigidBody.position.x + centerOffsetX) - (width / 2);
        myRight = (myObjectContainer.objectRigidBody.position.x + centerOffsetX) + (width / 2);
        myTop = (myObjectContainer.objectRigidBody.position.y + centerOffsetY) + (height / 2);
        myBottom = (myObjectContainer.objectRigidBody.position.y + centerOffsetY) - (height / 2);
    }

    /// <summary>
    /// Test for collision between two shapes
    /// </summary>
    /// <param name="otherCollider">Shape to test collision against</param>
    /// <returns>Bool collision</returns>
    public override bool CollisionTest(ObjectShape otherCollider)
    {
        if (otherCollider.GetType() == typeof(Rectangle))
        {
            return RectangleToRectangleCollisionTest(otherCollider.GetComponent<Rectangle>());
        }
        else if (otherCollider.GetType() == typeof(Circle))
        {
            return RectangleToCircleCollisionTest(otherCollider.GetComponent<Circle>());
        }
        return false;
    }

    /// <summary>
    /// Test for collision between two rectangles
    /// </summary>
    /// <param name="otherRectangle">Rectangle to test collision against</param>
    /// <returns>Bool collision</returns>
    public bool RectangleToRectangleCollisionTest(Rectangle otherRectangle)
    {
        return (myRight >= otherRectangle.myLeft && myLeft <= otherRectangle.myRight) && (myBottom < otherRectangle.myTop && myTop > otherRectangle.myBottom);
    }

    /// <summary>
    /// Test for collision between rectangle and cirle
    /// </summary>
    /// <param name="circle">Circle to test collision against</param>
    /// <returns>Bool collision</returns>
    public bool RectangleToCircleCollisionTest(Circle circle)
    {
        float nearestX = Mathf.Max(myLeft, Mathf.Min(circle.centerPoint.x, myRight));
        float nearestY = Mathf.Max(myBottom, Mathf.Min(circle.centerPoint.y, myTop));
        
        float deltaX = circle.centerPoint.x - nearestX;
        float deltaY = circle.centerPoint.y - nearestY;
        return (deltaX * deltaX + deltaY * deltaY) < (circle.radius * circle.radius);
    }
}
