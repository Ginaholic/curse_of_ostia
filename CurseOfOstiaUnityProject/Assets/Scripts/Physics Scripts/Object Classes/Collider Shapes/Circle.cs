﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Circle : ObjectShape
{
    public float radius;
    public Vector2 centerPoint;
    
    public override void Start()
    {
        base.Start();

        //Get initial shape center point
        UpdateCenterPoint();
    }

    public void Update()
    {
        //If game object is dynamic, recalculate bounds every frame
        if (myObjectContainer.objectRigidBody.isDynamic)
        {
            UpdateCenterPoint();
        }

        //Draw debug graphics
        if (DebugGraphicsController.collidersOn && myLineRenderer != null)
        {
            int segments = 50;

            myLineRenderer.useWorldSpace = false;
            myLineRenderer.positionCount = segments + 1;

            float x = centerPoint.y;
            float y = centerPoint.y;

            float change = (float)((2 * System.Math.PI) / segments);
            float angle = change;

            for (int i = 0; i < (segments + 1); i++)
            {
                x = Mathf.Sin(angle) * radius / 2;
                y = Mathf.Cos(angle) * radius / 2;

                myLineRenderer.SetPosition(i, new Vector3(x, y, -1));

                angle += change;
            }
        }
        else
        {
            myLineRenderer.positionCount = 0;
        }
    }

    /// <summary>
    /// Calculate the shape's center point 
    /// </summary>
    public void UpdateCenterPoint()
    {
        centerPoint = new Vector2(myObjectContainer.objectRigidBody.position.x + centerOffsetX, myObjectContainer.objectRigidBody.position.y + centerOffsetY);
    }

    /// <summary>
    /// Test for collision between two shapes
    /// </summary>
    /// <param name="otherCollider">Shape to test collision against</param>
    /// <returns>Bool collision</returns>
    public override bool CollisionTest(ObjectShape otherCollider)
    {
        if (otherCollider.GetType() == typeof(Circle))
        {
            return CircleToCircleCollisionTest(otherCollider.GetComponent<Circle>());
        }
        else if (otherCollider.GetType() == typeof(Rectangle))
        {
            return CircleToRectangleCollisionTest(otherCollider.GetComponent<Rectangle>());
        }
        return false;
    }

    /// <summary>
    /// Test for collision between two circles
    /// </summary>
    /// <param name="otherCircle">Circle to test collision against</param>
    /// <returns></returns>
    public bool CircleToCircleCollisionTest(Circle otherCircle)
    {
        float radiiSum = radius + otherCircle.radius;
        float deltaX = centerPoint.x + otherCircle.centerPoint.x;
        float deltaY = centerPoint.y + otherCircle.centerPoint.y;

        return (deltaX * deltaX) + (deltaY * deltaY) <= (radiiSum * radiiSum);
    }

    /// <summary>
    /// Test for collision between rectangle and circle
    /// </summary>
    /// <param name="rectangle"></param>
    /// <returns></returns>
    public bool CircleToRectangleCollisionTest(Rectangle rectangle)
    {
        float nearestX = Mathf.Max(rectangle.myLeft, Mathf.Min(centerPoint.x, rectangle.myRight));
        float nearestY = Mathf.Max(rectangle.myTop, Mathf.Min(centerPoint.y, rectangle.myBottom));
        
        float deltaX = centerPoint.x - nearestX;
        float deltaY = centerPoint.y - nearestY;
        return (deltaX * deltaX + deltaY * deltaY) < (radius * radius);
    }
}
