﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectRigidBody : MonoBehaviour
{
    public ObjectContainer myObjectContainer;

    public Vector2 velocity;
    public Vector2 position;
    public float mass;
    public bool isDynamic = false;

    void Awake()
    {
        position = transform.position;
    }

    void Start()
    {
        myObjectContainer = GetComponent<ObjectContainer>();
    }

    /// <summary>
    /// Apply a force to change rigidbody position
    /// </summary>
    /// <param name="forceX">Force in x axis</param>
    /// <param name="forceY">Force in y axis</param>
    public void ApplyForce(float forceX, float forceY)
    {
        velocity = new Vector2(forceX / mass, forceY / mass);
        position += new Vector2(velocity.x, velocity.y) * Time.deltaTime;
    }
    
    /// <summary>
    /// Update the gameobject's position to match it's rigidbody position
    /// </summary>
    public void UpdatePosition()
    {
        transform.position = position;
    }
}
