﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionResolutionController : MonoBehaviour
{
    /// <summary>
    /// Resolve collision between two objects
    /// </summary>
    /// <param name="collider1">Object 1</param>
    /// <param name="collider2">Object 2</param>
	public static void ResolveCollision(ObjectContainer collider1, ObjectContainer collider2)
    {
        if (collider1.tag == Constants.playerTag)
        {
            //If the player collides with a pick up item, collect the item
            if (collider2.tag == Constants.pickUpItemTag)
            {
                if (InventoryController.instance.PickUpItem(collider2.GetComponent<GameItemController>().thisItem))
                {
                    PhysicsObjectsContainerHolder.physicsObjectContainersToRemove.Add(collider2);
                    Destroy(collider2.gameObject);
                }
            }

            //If the player collides with an environment object, correct the player's position
            else if (collider2.tag == Constants.environmentObjectTag)
            {
                Vector2 correctedLocation = collider1.objectRigidBody.position;

                switch (GetCollisionSide(collider1, collider2))
                {
                    case Constants.collisionLeft:
                        correctedLocation.x -= Mathf.Abs(collider1.GetComponent<Rectangle>().myRight - collider2.GetComponent<Rectangle>().myLeft) - 0.01f;
                        break;
                    case Constants.collisionRight:
                        correctedLocation.x += Mathf.Abs(collider1.GetComponent<Rectangle>().myLeft - collider2.GetComponent<Rectangle>().myRight) + 0.01f;
                        break;
                    case Constants.collisionTop:
                        correctedLocation.y += Mathf.Abs(collider1.GetComponent<Rectangle>().myBottom - collider2.GetComponent<Rectangle>().myTop) + 0.01f;
                        break;
                    case Constants.collisionBottom:
                        correctedLocation.y -= Mathf.Abs(collider1.GetComponent<Rectangle>().myTop - collider2.GetComponent<Rectangle>().myBottom) - 0.01f;
                        break;
                }
                collider1.objectRigidBody.position = correctedLocation;
                collider1.objectRigidBody.UpdatePosition();
            }

            //If the player collides with a water area, apply the water force
            else if (collider2.tag == Constants.waterAreaTag)
            {
                Vector2 waterForce = collider2.GetComponent<WaterContoller>().waterForce;
                collider1.objectRigidBody.ApplyForce(waterForce.x, waterForce.y);
            }

            //If the player collides with an instant death area, modify the player's lives by -current lives
            else if (collider2.tag == Constants.instantDeathAreaTag)
            {
                collider1.GetComponent<PlayerController>().ModifyHealth(-collider1.GetComponent<PlayerController>().currentLives);
            }
        }
        
        else if (collider1.tag == Constants.arrowTag)
        {
            //If the arrow collides with an enemy NPC, apply the damage to the enemy and destroy the arrow's game object
            if (collider2.tag == Constants.enemyTag)
            {
                collider2.GetComponent<EnemyController>().ModifyHealth(-collider1.GetComponent<ArrowController>().currentDamage);
                PhysicsObjectsContainerHolder.physicsObjectContainersToRemove.Add(collider1);
                Destroy(collider1.gameObject);
            }

            //If the arrow collides with an environment object, destroy the arrow
            else if (collider2.tag == Constants.environmentObjectTag)
            {
                PhysicsObjectsContainerHolder.physicsObjectContainersToRemove.Add(collider1);
                Destroy(collider1.gameObject);
            }
        }

        else if (collider1.tag == Constants.bombTag)
        {

        }
    }

    /// <summary>
    /// Get which side the collision should be resolved on for a collision between a moving and static body
    /// </summary>
    /// <param name="collider1">Object 1 (moving)</param>
    /// <param name="collider2">Object 2 (static)</param>
    /// <returns>Int collision side</returns>
    public static int GetCollisionSide(ObjectContainer collider1, ObjectContainer collider2)
    {
        Rectangle collider1Rect = collider1.GetComponent<Rectangle>();
        Rectangle collider2Rect = collider2.GetComponent<Rectangle>();
        
        if (collider1Rect.myLeft < collider2Rect.myRight && collider1Rect.myRight >= collider2Rect.myRight)
        {
            if (collider1Rect.myBottom < collider2Rect.myTop && collider1Rect.myTop >= collider2Rect.myTop)
            {
                return Mathf.Abs(collider1.objectRigidBody.position.x - collider2.objectRigidBody.position.x) > Mathf.Abs(collider1.objectRigidBody.position.y - collider2.objectRigidBody.position.y) ? Constants.collisionRight : Constants.collisionTop;
            }
            else if (collider1Rect.myTop > collider2Rect.myBottom && collider1Rect.myBottom <= collider2Rect.myBottom)
            {
                return Mathf.Abs(collider1.objectRigidBody.position.x - collider2.objectRigidBody.position.x) > Mathf.Abs(collider1.objectRigidBody.position.y - collider2.objectRigidBody.position.y) ? Constants.collisionRight : Constants.collisionBottom;
            }
            return Constants.collisionRight;
        }

        else if (collider1Rect.myRight > collider2Rect.myLeft && collider1Rect.myLeft <= collider2Rect.myLeft)
        {      
            if (collider1Rect.myBottom < collider2Rect.myTop && collider1Rect.myTop >= collider2Rect.myTop)
            {
                return Mathf.Abs(collider1.objectRigidBody.position.x - collider2.objectRigidBody.position.x) > Mathf.Abs(collider1.objectRigidBody.position.y - collider2.objectRigidBody.position.y) ? Constants.collisionLeft : Constants.collisionTop;
            }
            else if (collider1Rect.myTop > collider2Rect.myBottom && collider1Rect.myBottom <= collider2Rect.myBottom)
            {
                return Mathf.Abs(collider1.objectRigidBody.position.x - collider2.objectRigidBody.position.x) > Mathf.Abs(collider1.objectRigidBody.position.y - collider2.objectRigidBody.position.y) ? Constants.collisionLeft : Constants.collisionBottom;
            }
            return Constants.collisionLeft;
        }

        else if (collider1Rect.myBottom < collider2Rect.myTop && collider1Rect.myTop >= collider2Rect.myTop)
        {
            return Constants.collisionTop;
        }
        else if (collider1Rect.myTop > collider2Rect.myBottom && collider1Rect.myBottom <= collider2Rect.myBottom)
        {
            return Constants.collisionBottom;
        }
        
        return Constants.collisionNone;
    }
}
