﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PlayerController : MonoBehaviour
{    
    public static PlayerController instance;

    private ObjectContainer myObjectContainer;
    private Animator myAnimator;
    
    public GameObject enemyHolder;

    public Image powerFillBar;
    public GameObject livesDisplay;

    private float maxLives = Constants.playerMaxLives;

    public int maxMeleeDamage = Constants.playerMaxMeleeDamage;
    public float maxMeleeRange = Constants.playerMaxMeleeRange;
    public float speed = Constants.playerSpeed;
    public float attackPower = 0;
    public float currentLives;

    //Default direction is up because of the way the character is loaded into the scene
    public string lastFacedDirection = Constants.playerUp;

    void Awake()
    {
        MakeInstance();

        myObjectContainer = GetComponent<ObjectContainer>();
        myAnimator = GetComponent<Animator>();
    }

    void Start()
    {
        currentLives = maxLives;
    }

    void Update()
    {
        PlayerInput();
    }


    /// <summary>
    /// Initialise controller instance
    /// </summary>
    void MakeInstance()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    /// <summary>
    /// Check for player input and act accordingly
    /// </summary>
    public void PlayerInput()
    {
        if (!EventSystem.current.IsPointerOverGameObject())
        {
            float horizontalInput = Input.GetAxisRaw(Constants.horizontalInput);
            float verticalInput = Input.GetAxisRaw(Constants.verticalInput);

            float primaryAttackInput = Input.GetAxisRaw(Constants.primaryAttackInput);
            float secondaryAttackInput = Input.GetAxisRaw(Constants.secondaryAttackInput);

            bool primaryAttackRelease = Input.GetButtonUp(Constants.primaryAttackInput);
            bool secondaryAttackRelease = Input.GetButtonUp(Constants.secondaryAttackInput);
            
            if (horizontalInput != 0 || verticalInput != 0)
            {
                Move(horizontalInput, verticalInput);
            }
            else if (primaryAttackInput != 0 || secondaryAttackInput != 0)
            {
                Attack(primaryAttackInput, secondaryAttackInput);
            }
            else if (primaryAttackRelease || secondaryAttackRelease)
            {
                if (primaryAttackRelease)
                {
                    Shoot();
                }
                else if (secondaryAttackRelease && InventoryController.instance.HasBombs())
                {
                    ThrowBomb();
                }
                else
                {
                    Punch();
                }
                
                ClearAttackPower();
            }
            else
            {
                myAnimator.Play(Constants.animatorIdleState);
            }
        }
        else
        {
            //Stop animations when using UI 
            myAnimator.Play(Constants.animatorIdleState);
        }
    }

    /// <summary>
    /// Move player and hide power bar
    /// </summary>
    /// <param name="horizontalInput">Horizontal input axis</param>
    /// <param name="verticalInput">Veritcal input axis</param>
    public void Move(float horizontalInput, float verticalInput)
    {
        myAnimator.Play(Constants.animatorWalkState);
        myAnimator.SetFloat(Constants.animatorFaceXFloat, horizontalInput);
        myAnimator.SetFloat(Constants.animatorFaceYFloat, verticalInput);
        
        ClearAttackPower();
        
        if (horizontalInput != 0)
        {
            float forceY = 0;
            float forceX;

            if (horizontalInput > 0)
            {
                forceX = speed;
                lastFacedDirection = Constants.playerRight;
            }
            else
            {
                forceX = -speed;
                lastFacedDirection = Constants.playerLeft;
            }

            myObjectContainer.objectRigidBody.ApplyForce(forceX, forceY);
        }
        
        if (verticalInput != 0)
        {
            float forceX = 0;
            float forceY;

            if (verticalInput > 0)
            {
                forceY = speed;
                lastFacedDirection = Constants.playerUp;
            }
            else
            {
                forceY = -speed;
                lastFacedDirection = Constants.playerDown;
            }

            myObjectContainer.objectRigidBody.ApplyForce(forceX, forceY);
        }
    }

    /// <summary>
    /// Start attack and ramp up attack power
    /// </summary>
    /// <param name="primaryAttackInput">Primary attack input axis</param>
    /// <param name="secondaryAttackInput">Secondary attack input axis</param>
    public void Attack(float primaryAttackInput, float secondaryAttackInput)
    {
        if (primaryAttackInput != 0)
        {
            myAnimator.Play(Constants.playerShootState);
        }
        else if (secondaryAttackInput != 0)
        {
            myAnimator.Play(Constants.playerPunchState);
        }
        
        transform.GetChild(0).gameObject.SetActive(true);
        IncreaseAttackPower();
    }

    /// <summary>
    /// Increase attack power until it reaches 1, update UI power bar
    /// </summary>
    public void IncreaseAttackPower()
    {
        if (attackPower < 1)
        {
            attackPower = attackPower + 0.02f;
            powerFillBar.fillAmount = attackPower;
        }
    }

    /// <summary>
    /// Reset attack power to 0 and hide UI power bar
    /// </summary>
    public void ClearAttackPower()
    {
        attackPower = 0;
        powerFillBar.fillAmount = attackPower;
        transform.GetChild(0).gameObject.SetActive(false);
    }
    
    /// <summary>
    /// Spawn arrow game object and fire
    /// </summary>
    public void Shoot()
    {
        string arrowPrefabPath = Constants.arrowUp;

        if (lastFacedDirection == Constants.playerDown)
        {
            arrowPrefabPath = Constants.arrowDown;
        }
        else if (lastFacedDirection == Constants.playerLeft)
        {
            arrowPrefabPath = Constants.arrowLeft;
        }
        else if (lastFacedDirection == Constants.playerRight)
        {
            arrowPrefabPath = Constants.arrowRight;
        }
        
        GameObject arrowPrefab = Resources.Load(Constants.weaponsPrefabFolder + arrowPrefabPath) as GameObject;
        GameObject arrowGameObject = Instantiate(arrowPrefab, transform.position, arrowPrefab.transform.rotation);
        
        arrowGameObject.GetComponent<ArrowController>().fire = true;
        arrowGameObject.GetComponent<ArrowController>().SetRangeAndDamage(attackPower);
    }

    /// <summary>
    /// Create punch game object
    /// <summary>
    public void Punch()
    {
        string punchPrefabPath = Constants.punchUp;
        Vector3 punchPosition = new Vector3 (transform.position.x - 0.1f, transform.position.y + 0.2f, 0);

        if (lastFacedDirection == Constants.playerDown)
        {
            punchPrefabPath = Constants.punchDown;
            punchPosition = new Vector3(transform.position.x + 0.5f, transform.position.y - 0.6f, 0);
        }
        else if (lastFacedDirection == Constants.playerLeft)
        {
            punchPrefabPath = Constants.punchLeft;
            punchPosition = new Vector3(transform.position.x - 0.3f, transform.position.y - 0.5f, 0);
        }
        else if (lastFacedDirection == Constants.playerRight)
        {
            punchPrefabPath = Constants.punchRight;
            punchPosition = new Vector3(transform.position.x + 0.6f, transform.position.y - 0.5f, 0);
        }
        
        GameObject punchPrefab = Resources.Load(Constants.weaponsPrefabFolder + punchPrefabPath) as GameObject;
        GameObject punchGameObject = Instantiate(punchPrefab, punchPosition, punchPrefab.transform.rotation);

        punchGameObject.GetComponent<PunchController>().SetDamage(attackPower);
    }

    /// <summary>
    /// Create bomb game object and throw
    /// </summary>
    public void ThrowBomb()
    {
        //Create an bomb game object at the players location and throw
        //GameObject bomb = Instantiate(Resources.Load("Bomb") as GameObject, transform);
        //bomb.GetComponent<BombController>().throw = true;
    }

    /// <summary>
    /// Get all enemy transforms
    /// </summary>
    /// <returns>List of all enemy transforms</returns>
    public List<Transform> GetEnemies()
    {
        List<Transform> enemies = new List<Transform>();

        for (int i = 0; i < enemyHolder.transform.childCount; i++)
        {
            enemies.Add(enemyHolder.transform.GetChild(i));
        }

        return enemies;
    }

    /// <summary>
    /// Alter lives, update UI lives display or die
    /// </summary>
    /// <param name="amount"></param>
    public void ModifyHealth(float amount)
    {
        currentLives += amount;
        
        if (amount < 0)
        {
            GameplayController.instance.LoseLife();
        }
        
        if (currentLives > maxLives)
        {
            currentLives = maxLives;
        }

        int numberOfFullHearts = Mathf.FloorToInt(currentLives);
        int numberOfHalfHearts = Mathf.CeilToInt(currentLives - numberOfFullHearts);
        int numberOfEmptyHearts = (int)maxLives - (numberOfFullHearts + numberOfHalfHearts);
            
        for (int i = 0; i < livesDisplay.transform.childCount; i++)
        {
            Destroy(livesDisplay.transform.GetChild(i).gameObject);
        }

        for (int i = 0; i < numberOfFullHearts; i++)
        {
            GameObject fullHeart = Instantiate(Resources.Load(Constants.fullHeartPrefabPath) as GameObject);
            fullHeart.transform.SetParent(livesDisplay.transform);
            fullHeart.transform.localScale = new Vector3(1, 1, 1);
        }

        for (int i = 0; i < numberOfHalfHearts; i++)
        {
            GameObject halfHeart = Instantiate(Resources.Load(Constants.halfHeartPrefabPath) as GameObject);
            halfHeart.transform.SetParent(livesDisplay.transform);
            halfHeart.transform.localScale = new Vector3(1, 1, 1);
        }

        for (int i = 0; i < numberOfEmptyHearts; i++)
        {
            GameObject emptyHeart = Instantiate(Resources.Load(Constants.emptyHeartPrefabPath) as GameObject);
            emptyHeart.transform.SetParent(livesDisplay.transform);
            emptyHeart.transform.localScale = new Vector3(1, 1, 1);
        }

        if (currentLives <= 0)
        {
            MenuController.instance.ReturnToMainMenu();
        }
    }
}
