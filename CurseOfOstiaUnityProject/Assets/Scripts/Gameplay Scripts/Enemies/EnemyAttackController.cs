﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttackController : MonoBehaviour
{    
    void Start()
    {
        StartCoroutine(DestroyThis());
    }

    /// <summary>
    /// Destroy this after *1* second
    /// </summary>
    /// <returns>Seconds waited</returns>
    IEnumerator DestroyThis()
    {
        yield return new WaitForSecondsRealtime(Constants.enemyAttackDisplayTime);
        Destroy(gameObject);
    }
}
