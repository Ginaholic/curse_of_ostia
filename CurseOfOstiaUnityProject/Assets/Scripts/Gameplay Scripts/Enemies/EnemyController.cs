﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyController : MonoBehaviour
{
    private ObjectContainer myObjectContainer;
    public Animator myAnimator;

    public Image healthFillBar;
    public string enemyType;

    private float maxHealth = Constants.enemyMaxHealth;
    private float attackDamage = Constants.enemyAttackDamage;
    
    public float speed = Constants.enemySpeed;
    public float attackRange = Constants.enemyAttackRange;
    public float attackTimerMin = Constants.enemyAttackTimerMin;
    public float visionRange = Constants.enemyVisionRange;
    public float visionMaxAngle = Constants.enemyVisionMaxAngle;
    public float visionMinAngle = Constants.enemyVisionMinAngle;
    public float currentHealth;

    public bool isDead = false;
    
    void Awake()
    {
        myObjectContainer = GetComponent<ObjectContainer>();
        myAnimator = GetComponent<Animator>();
    }

    void Start()
    {
        currentHealth = maxHealth;
    }

    /// <summary>
    /// Move in specified direction
    /// </summary>
    /// <param name="direction">Direction to move in</param>
    public void Move(Vector3 direction)
    {
        myAnimator.Play(Constants.animatorWalkState);
        myAnimator.SetFloat(Constants.animatorFaceXFloat, direction.x);
        myAnimator.SetFloat(Constants.animatorFaceYFloat, direction.y);
        
        myObjectContainer.objectRigidBody.ApplyForce(direction.x * speed, direction.y * speed);
    }

    /// <summary>
    /// Attack, create attack animation gameobject facing direction specified and deal damage to player
    /// </summary>
    /// <param name="directionToAttackIn">Direction to attack in</param>
    public void Attack(string directionToAttackIn)
    {
        myAnimator.Play(Constants.animatorIdleState);

        string attackPrefabPath = Constants.enemyAttackUp;
        Vector3 attackPosition = new Vector3(transform.position.x, transform.position.y, 0);
        float faceX = 0;
        float faceY = 1;

        if (directionToAttackIn == Constants.enemyAttackDown)
        {
            faceX = 0;
            faceY = -1;

            attackPrefabPath = Constants.enemyAttackDown;
            attackPosition = new Vector3(transform.position.x, transform.position.y, 0);
        }
        else if (directionToAttackIn == Constants.enemyAttackLeft)
        {
            faceX = -1;
            faceY = 0;

            attackPrefabPath = Constants.enemyAttackLeft;
            attackPosition = new Vector3(transform.position.x, transform.position.y, 0);
        }
        else if (directionToAttackIn == Constants.enemyAttackRight)
        {
            faceX = 1;
            faceY = 0;

            attackPrefabPath = Constants.enemyAttackRight;
            attackPosition = new Vector3(transform.position.x, transform.position.y, 0);
        }

        myAnimator.SetFloat(Constants.animatorFaceXFloat, faceX);
        myAnimator.SetFloat(Constants.animatorFaceYFloat, faceY);

        GameObject attackPrefab = Resources.Load(Constants.enemiesPrefabFolder + attackPrefabPath) as GameObject;
        GameObject attackGameObject = Instantiate(attackPrefab, attackPosition, attackPrefab.transform.rotation);
        
        PlayerController.instance.ModifyHealth(-attackDamage);
    }

    /// <summary>
    /// Die, if not already dead. Hide health bar, spawn random item and update kill count
    /// </summary>
    public void Die()
    {
        if (!isDead)
        {
            isDead = true;

            transform.GetChild(0).gameObject.SetActive(false);
            GameplayController.instance.SpawnRandomItem(transform.position);

            myAnimator.Play(Constants.enemyDieState);
            StartCoroutine(WaitThenDestroy());

            if (enemyType == Constants.enemyTypeMoleman)
            {
                StatsController.IncreaseMolemenKillCount();
            }
            else if (enemyType == Constants.enemyTypeTreant)
            {
                StatsController.IncreaseTreantKillCount();
            }
        }
    }

    /// <summary>
    /// Alter health, update health bar UI or die
    /// </summary>
    /// <param name="amount">Amount to alter health by</param>
    public void ModifyHealth(float amount)
    {
        currentHealth += amount;

        if (currentHealth <= 0)
        {
            Die();
        }
        else
        {
            healthFillBar.fillAmount = currentHealth / maxHealth;
        }
    }

    /// <summary>
    /// Wait for death animation to finish then destroy
    /// </summary>
    /// <returns>Seconds waited</returns>
    IEnumerator WaitThenDestroy()
    {
        yield return new WaitForSeconds(Constants.enemyDeadDisplayTime);
        PhysicsObjectsContainerHolder.physicsObjectContainersToRemove.Add(myObjectContainer);
        Destroy(gameObject);
    }
}
