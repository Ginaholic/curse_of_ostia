﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowController : MonoBehaviour
{
    private ObjectContainer myObjectContainer;
    private Vector3 spawnLocation;

    public bool fire = false;
    public bool homing = false;

    private int maxRange = BowTierController.activeBowTier.maxRange;
    private int maxDamage = ArrowTierController.activeArrowTier.maxDamage;
    private int maxSpeed = Constants.arrowMaxSpeed;

    public float currentRange;
    public float currentDamage;

    public List<Node> currentPath;
    public GameObject pathTarget;
    public int pathIndex = 0;
    public LineRenderer myPathLineRenderer;

    void Awake()
    {
        myObjectContainer = GetComponent<ObjectContainer>();
    }

    void Start()
    {
        name = name.Replace(Constants.nameCloneSuffix, Constants.empty);

        //If current arrow tier is tier 2, arrow is homing
        if (ArrowTierController.activeArrowTier.id == Constants.upgradeTier2)
        {
            homing = true;

            Transform nearestEnemy = null;
            float nearestDistance = 9999999f;
            
            foreach (Transform enemyTransform in PlayerController.instance.GetEnemies())
            {
                Vector3 directionToEnemy = enemyTransform.position - PlayerController.instance.transform.position;
                
                if ((name == Constants.arrowDown && directionToEnemy.y < 0) || (name == Constants.arrowUp && directionToEnemy.y > 0) || (name == Constants.arrowLeft && directionToEnemy.x < 0) || (name == Constants.arrowRight && directionToEnemy.x > 0))
                {
                    float distanceToEnemy = Vector3.Distance(enemyTransform.position, PlayerController.instance.transform.position);
                    
                    if (distanceToEnemy < nearestDistance)
                    {
                        nearestDistance = distanceToEnemy;
                        nearestEnemy = enemyTransform;
                    }
                }
            }
            
            if (nearestEnemy != null)
            {
                pathTarget = nearestEnemy.gameObject;
                pathIndex = 0;

                currentPath = Pathfinding.instance.FindPath(gameObject, pathTarget);
            }
            else
            {
                PhysicsObjectsContainerHolder.physicsObjectContainersToRemove.Add(myObjectContainer);
                Destroy(gameObject);
            }
        }

        spawnLocation = transform.position;
    }

    void Update()
    {
        //Update homing arrow path
        if (homing)
        {
            //Draw debug graphics
            if (DebugGraphicsController.pathsOn)
            {
                if (myPathLineRenderer != null && currentPath.Count > 0)
                {
                    myPathLineRenderer.positionCount = currentPath.Count;

                    for (int i = 0; i < currentPath.Count; i++)
                    {
                        myPathLineRenderer.SetPosition(i, new Vector3(currentPath[i].transform.position.x, currentPath[i].transform.position.y, -1));
                    }
                }
            }
            else
            {
                myPathLineRenderer.positionCount = 0;
            }
            
            if (Vector2.Distance(transform.position, currentPath[pathIndex].transform.position) < Constants.distanceToPoint)
            {
                pathIndex++;
                
                if (pathIndex >= currentPath.Count)
                {
                    pathIndex = 0;

                    currentPath = Pathfinding.instance.FindPath(gameObject, pathTarget);
                    
                    if (currentPath == null || currentPath.Count <= 0)
                    {
                        PhysicsObjectsContainerHolder.physicsObjectContainersToRemove.Add(myObjectContainer);
                        Destroy(gameObject);
                    }
                }
            }
            
            Vector3 directionToPathPoint = currentPath[pathIndex].transform.position - transform.position;
            myObjectContainer.objectRigidBody.ApplyForce(directionToPathPoint.x * maxSpeed, directionToPathPoint.y * maxSpeed);
            
            transform.up = pathTarget.transform.position - transform.position; 
        }
        else if (fire)
        {
            Fire();
            ShotDistance();
        }
    }
    
    /// <summary>
    /// Set arrow damage and range when instantiated
    /// </summary>
    /// <param name="playersAttackPower">Player attack power</param>
    public void SetRangeAndDamage(float playersAttackPower)
    {
        currentDamage = maxDamage;
        currentRange = playersAttackPower * maxRange;
    }

    /// <summary>
    /// Move arrow
    /// </summary>
    public void Fire()
    {
        float forceX = 0;
        float forceY = 0;
        
        if (name == Constants.arrowDown)
        {
            forceY = -maxSpeed;
        }
        else if (name == Constants.arrowUp)
        {
            forceY = maxSpeed;
        }
        else if (name == Constants.arrowLeft)
        {
            forceX = -maxSpeed;
        }
        else if (name == Constants.arrowRight)
        {
            forceX = maxSpeed;
        }

        //Apply force to arrow's object rigidbody
        myObjectContainer.objectRigidBody.ApplyForce(forceX, forceY);
    }

    /// <summary>
    /// Destroy arrow once it flies past max range
    /// </summary>
    public void ShotDistance()
    {
        if (Vector3.Distance(spawnLocation, transform.position) >= currentRange)
        {
            PhysicsObjectsContainerHolder.physicsObjectContainersToRemove.Add(myObjectContainer);
            Destroy(gameObject);
            fire = false;
        }
    }
}