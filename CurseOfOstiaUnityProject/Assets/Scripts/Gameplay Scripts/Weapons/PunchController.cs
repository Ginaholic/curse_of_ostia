﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PunchController : MonoBehaviour
{    
    private int maxDamage = PlayerController.instance.maxMeleeDamage;
    private float currentDamage;

    void Start ()
    {
        name = name.Replace(Constants.nameCloneSuffix, Constants.empty);

        Transform nearestEnemy = null;
        float nearestDistance = 9999999f;
        
        foreach (Transform enemyTransform in PlayerController.instance.GetEnemies())
        {
            Vector3 directionToEnemy = enemyTransform.position - PlayerController.instance.transform.position;
            
            if ((name == Constants.punchDown && directionToEnemy.y < 0) || (name == Constants.punchUp && directionToEnemy.y > 0) || (name == Constants.punchLeft && directionToEnemy.x < 0) || (name == Constants.punchRight && directionToEnemy.x > 0))
            {
                float distanceToEnemy = Vector3.Distance(enemyTransform.position, PlayerController.instance.transform.position);

                if (distanceToEnemy <= PlayerController.instance.maxMeleeRange)
                {
                    if (distanceToEnemy < nearestDistance)
                    {
                        nearestDistance = distanceToEnemy;
                        nearestEnemy = enemyTransform;
                    }
                }
            }
        }
        
        if (nearestEnemy != null)
        {
            nearestEnemy.GetComponent<EnemyController>().ModifyHealth(-currentDamage);
        }
        
        StartCoroutine(DestroyThis());
    }

    /// <summary>
    /// Set punch damage
    /// </summary>
    /// <param name="playersAttackPower">Players attack damage</param>
    public void SetDamage(float playersAttackPower)
    {
        currentDamage = playersAttackPower * maxDamage;
    }

    /// <summary>
    /// Wait for punch animation to end then destroy this
    /// </summary>
    /// <returns>Seconds waited</returns>
    IEnumerator DestroyThis()
    {
        yield return new WaitForSecondsRealtime(Constants.punchDisplayTime);
        Destroy(gameObject);
    }
}
