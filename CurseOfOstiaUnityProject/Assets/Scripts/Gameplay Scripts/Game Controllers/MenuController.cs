﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{
    public static MenuController instance;
    
    void Awake()
    {
        MakeInstance();
    }

    /// <summary>
    /// Initialise controller instance
    /// </summary>
    void MakeInstance()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    /// <summary>
    /// Load level scene
    /// </summary>
    public void StartGame()
    {
        SceneFaderController.instance.LoadLevel(Constants.levelScene);
    }

    /// <summary>
    /// Load achievements scene
    /// </summary>
    public void AchievementsMenu()
    {
        SceneFaderController.instance.LoadLevel(Constants.achievementsScene);
    }
    
    /// <summary>
    /// Load options scene
    /// </summary>
    public void OptionsMenu()
    {
        SceneFaderController.instance.LoadLevel(Constants.optionsScene);
    }

    /// <summary>
    /// Load main menu scene
    /// </summary>
    public void ReturnToMainMenu()
    {
        SceneFaderController.instance.LoadLevel(Constants.mainMenuScene);
    }

    /// <summary>
    /// Quit application
    /// </summary>
    public void QuitGame()
    {
        Application.Quit();
    }
}
