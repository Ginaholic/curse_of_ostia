﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RendererPositionSorter : MonoBehaviour
{    
    private int sortingOrderBase = Constants.sortingOrderBase;
    private float timer;
    private float timerMax = 0.1f;
    
    private Renderer myRenderer;

    void Awake()
    {
        myRenderer = GetComponent<Renderer>();
        timer = timerMax;
    }

    void LateUpdate()
    {
        //Once all other actions have been perfomed in update, change the sorting order of objects based on their y position
        //Do this once every 1000ms to save performance
        timer = -Time.deltaTime;
        if (timer <= 0)
        {
            myRenderer.sortingOrder = sortingOrderBase - Mathf.RoundToInt(transform.position.y);
            timer = timerMax;

            //Non-dynamic object's sorting order does not need to be calculated more than once, so remove this component from the object
            if (!GetComponent<ObjectRigidBody>().isDynamic)
            {
                Destroy(this);
            }
        }
    }
}
