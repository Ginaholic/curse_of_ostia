﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventorySlot
{
    public GameItem item;
    public int count;

    public InventorySlot(GameItem _item, int _count)
    {
        item = _item;
        count = _count;
    }

    public InventorySlot()
    {
        item = null;
        count = Constants.defaultEmptySlotCount;
    }

    public void Use()
    {
        //If the item is a bomb or emelyn item, don't alter count
        if (!(GameItem.CheckIfEmelynItem(item.name) || item.name == "Bomb"))
        {
            //Reduce the item count
            count = count - 1;
        }
        
        item.UseItem();
        //If the count is no 0, empty the slot
        if (count == 0)
        {
            item = null;
        }
    }

    public void PickUp()
    {
        //Increase the item count
        count = count + 1;
    }
}
