﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameItemController : MonoBehaviour
{    
    public GameItem thisItem;
    
    void Awake ()
    {
        //As this game object is likely to be instantiated using resources.load, remove the "(Clone)" part of its name before creating it as a GameItem
        name = name.Replace(Constants.nameCloneSuffix, Constants.empty);
        thisItem = new GameItem(gameObject.name, gameObject.GetComponent<SpriteRenderer>().sprite);
	}
}
