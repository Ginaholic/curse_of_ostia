﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameItem
{
    public string name;
    public string effectDescription;
    public int maxCount;
    public Sprite sprite;

    public GameItem(string _name, Sprite _sprite)
    {
        name = _name;
        effectDescription = SetItemEffectDescription(_name);
        maxCount = CheckIfEmelynItem(_name) ? Constants.emylynItemMaxItemCount : Constants.defaultMaxItemCount;
        sprite = _sprite;
    }

    public static bool CheckIfEmelynItem(string itemName)
    {
        if (itemName ==  Constants.emelynNecklaceItemName || itemName == Constants.emelynStaffItemName || itemName == Constants.emelynScrollItemName || itemName == Constants.emelynBookItemName)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void UseItem()
    {
        //Display a message with the item's description
        GameplayController.instance.ItemUseDescription(this);

        //Apply the item's effect
        if (name == Constants.healthPotionItemName)
        {
            PlayerController.instance.ModifyHealth(1);
        }
        else if (name == Constants.staminaPotionItemName)
        {
            //PlayerController.instance.IncreaseSpeed();
        }
        else if (name == Constants.bowBook1ItemName)
        {
            UpgradesController.instance.UnlockTier(1, Constants.bowUpgradeTier);
        }
        else if (name == Constants.bowBook2ItemName)
        {
            UpgradesController.instance.UnlockTier(2, Constants.bowUpgradeTier);
        }
        else if (name == Constants.arrowBook1ItemName)
        {
            UpgradesController.instance.UnlockTier(1, Constants.arrowUpgradeTier);
        }
        else if (name == Constants.arrowBook2ItemName)
        {
            UpgradesController.instance.UnlockTier(2, Constants.arrowUpgradeTier);
        }
        else if (name == Constants.appleItemName)
        {
            //PlayerController.instance.IncreasePower();
        }
        else if (name == Constants.cherryItemName)
        {
            //PlayerController.instance.SetInvisible();
        }
    }

    public static string SetItemEffectDescription(string itemName)
    {
        //Set the description based on the item
        string description = null;

        if (itemName == Constants.healthPotionItemName)
        {
            description = Constants.healthPotionItemDescription;
        }
        else if (itemName == Constants.staminaPotionItemName)
        {
            description = Constants.staminaPotionItemDescription;
        }
        else if (itemName == Constants.bowBook1ItemName)
        {
            description = Constants.bowBook1ItemDescription;
        }
        else if (itemName == Constants.bowBook2ItemName)
        {
            description = Constants.bowBook2ItemDescription;
        }
        else if (itemName == Constants.arrowBook1ItemName)
        {
            description = Constants.arrowBook1ItemDescription;
        }
        else if (itemName == Constants.arrowBook2ItemName)
        {
            description = Constants.arrowBook2ItemDescription;
        }
        else if (itemName == Constants.appleItemName)
        {
            description = Constants.appleItemDescription;
        }
        else if (itemName == Constants.cherryItemName)
        {
            description = Constants.cherryItemDescription;
        }

        return description;
    }
}
