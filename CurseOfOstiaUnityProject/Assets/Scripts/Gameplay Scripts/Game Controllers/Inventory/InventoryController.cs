﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryController : MonoBehaviour
{
    public static InventoryController instance;

    public GameObject[] slotGameObjects;
    public static InventorySlot[] inventorySlots = new InventorySlot[10];

    void Awake()
    {
        MakeInstance();
    }

    void Start()
    {
        InitialiseInventorySlots();
    }

    void MakeInstance()
    {
        //Initialise the instance of this controller if it has not already been done
        if (instance == null)
        {
            instance = this;
        }
    }

    public void UpdateSlots()
    {
        SortSlots();

        for (int i = 0; i < inventorySlots.Length; i++)
        {
            SetSlotInfo(i);
        }
    }

    public void SortSlots()
    {
        //Reorder inventory slots by count
        System.Array.Sort(inventorySlots, delegate (InventorySlot slot1, InventorySlot slot2) 
        {
            return -slot1.count.CompareTo(slot2.count);
        });
    }

    public void SetSlotInfo(int slotIndex)
    {
        //Set the slot game objects image sprite and count text
        if (inventorySlots[slotIndex].item != null)
        {
            slotGameObjects[slotIndex].transform.GetChild(0).GetComponent<Image>().sprite = inventorySlots[slotIndex].item.sprite;
            slotGameObjects[slotIndex].transform.GetChild(1).GetComponent<Text>().text = inventorySlots[slotIndex].count.ToString();
            slotGameObjects[slotIndex].transform.GetChild(0).GetComponent<Image>().enabled = true;
            slotGameObjects[slotIndex].transform.GetChild(1).GetComponent<Text>().enabled = true;
        }
        else
        {
            slotGameObjects[slotIndex].transform.GetChild(0).GetComponent<Image>().enabled = false;
            slotGameObjects[slotIndex].transform.GetChild(1).GetComponent<Text>().enabled = false;
        }
    }

    public bool PickUpItem(GameItem item)
    {
        //Check if the item is a gem
        if (item.name == Constants.purpleGemItemName || item.name == Constants.blueGemItemName || item.name == Constants.silverGemItemName)
        {
            //If it is, increase currency value
            UpgradesController.instance.GainCurrency(Constants.gemRewardModifier + item.name);
            return true;
        }
        else
        {
            //If it isn't, check if there is a slot already holding this item
            InventorySlot result1 = null;
            foreach (InventorySlot slot in inventorySlots)
            {
                if (slot.item != null && slot.item.name == item.name)
                {
                    result1 = slot;
                }
            }

            if (result1 != null)
            {
                //If there is, check if it's count is the items max count
                if (result1.count == result1.item.maxCount)
                {
                    //If it is, tell the player that they can't carry any more of this item
                    GameplayController.instance.ItemLimit();
                    return false;
                }
                else
                {
                    //If it isn't, pick the item up and update the inventory slots
                    result1.PickUp();
                    GameplayController.instance.PickUpItem();
                    UpdateSlots();
                    return true;
                }
            }
            else
            {
                //If this item isn't already in the inventory, check if there is an empty space
                int result2 = System.Array.FindIndex(inventorySlots, slot => slot.item == null);

                if (result2 != -1)
                {
                    //If there is a space, create a new slot and update the slots
                    inventorySlots[result2] = new InventorySlot(item, Constants.defaultNewSlotCount);
                    GameplayController.instance.PickUpItem();
                    UpdateSlots();

                    if (GameItem.CheckIfEmelynItem(item.name))
                    {
                        //If the item is an Emelyn item, update the stats tracker
                        StatsController.IncreaseEmelynItemCount();
                    }
                    return true;
                }
                else
                {
                    //If there isn't, tell the player their bag is full
                    GameplayController.instance.BagFull();
                    return false;
                }
            }
        }
    }

    public void UseItem(int slotIndex)
    {
        //Use the item, and update the list
        if (inventorySlots[slotIndex].item != null)
        {
            inventorySlots[slotIndex].Use();
            UpdateSlots();
        }
    }

    public bool HasBombs()
    {
        //Check if there is a slot holding a bomb
        InventorySlot result1 = null;
        foreach (InventorySlot slot in inventorySlots)
        {
            if (slot.item != null && slot.item.name == Constants.bombItemName)
            {
                result1 = slot;
            }
        }

        if (result1 != null)
        {
            //If there is, the player has at least 1 bomb so return true
            return true;
        }

        return false;
    }

    public static void InitialiseInventorySlots()
    {
        //Initialise all slots as empty
        for (int i = 0; i < inventorySlots.Length; i++)
        {
            inventorySlots[i] = new InventorySlot();
        }
    }
}
