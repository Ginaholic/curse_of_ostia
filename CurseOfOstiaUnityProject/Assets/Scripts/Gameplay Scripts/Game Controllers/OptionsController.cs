﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsController : MonoBehaviour
{
    public Toggle onButton;
    public Toggle offButton;
    public Slider musicSlider;
    public Slider sfxSlider;

    void Start()
    {
        //Set settings using game preferences
        if (MusicController.instance != null && MusicController.instance.CheckWhetherToPlay())
        {
            onButton.isOn = true;
            offButton.isOn = false;
        }
        else
        {
            onButton.isOn = false;
            offButton.isOn = true;
        }

        musicSlider.normalizedValue = OptionsPreferences.GetMusicVolume();
        sfxSlider.normalizedValue = OptionsPreferences.GetSFXVolume();
    }

    /// <summary>
    /// Set music preference to off, stop music and update UI button state
    /// </summary>
    public void SoundOff()
    {
        if (offButton.isOn)
        {
            onButton.isOn = false;
            offButton.isOn = true;
            OptionsPreferences.SetSoundState(0);
            if (MusicController.instance != null)
            {
                MusicController.instance.StopMusic();
            }
        }
    }

    /// <summary>
    /// Set music preference to on, play music and update UI button state
    /// </summary>
    public void SoundOn()
    {
        if (onButton.isOn)
        {
            onButton.isOn = true;
            offButton.isOn = false;
            OptionsPreferences.SetSoundState(1);
            if (MusicController.instance != null)
            {
                MusicController.instance.PlayMusic();
            }
        }
    }

    /// <summary>
    /// Change music volume
    /// </summary>
    /// <param name="volume">New volume</param>
    public void ChangeMusicVolume(float volume)
    {
        if (MusicController.instance != null)
        {
            MusicController.instance.SetMusicVolume(volume);
        }
        OptionsPreferences.SetMusicVolume(volume);
    }

    /// <summary>
    /// Change sfx volume
    /// </summary>
    /// <param name="volume">New volume</param>
    public void ChangeSFXVolume(float volume)
    {
        if (MusicController.instance != null)
        {
            MusicController.instance.SetSFXVolume(volume);
        }
        OptionsPreferences.SetSFXVolume(volume);
    }
}
