﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneFaderController : MonoBehaviour
{
    public static SceneFaderController instance;
    
    public GameObject fadePanel;
    public Animator fadeAnim;

    void Awake()
    {
        MakeSingleton();
    }
    
    /// <summary>
    /// Initialise controller singleton
    /// </summary>
    void MakeSingleton()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    /// <summary>
    /// Load level with fade animation
    /// </summary>
    /// <param name="level">Name of level to load</param>
    public void LoadLevel(string level)
    {
        StartCoroutine(FadeInOut(level));
    }

    /// <summary>
    /// Create fade effect between scenes 
    /// </summary>
    /// <param name="level">Name of level to load</param>
    /// <returns>Seconds waited</returns>
    IEnumerator FadeInOut(string level)
    {
        fadePanel.SetActive(true);
        fadeAnim.Play(Constants.sceneFadeOutState);

        yield return new WaitForSeconds(Constants.sceneFadeOutDisplayTime);

        SceneManager.LoadScene(level);
        fadeAnim.Play(Constants.sceneFadeInState);

        yield return new WaitForSeconds(Constants.sceneFadeInDisplayTime);
       
        fadePanel.SetActive(false);
    }
}
