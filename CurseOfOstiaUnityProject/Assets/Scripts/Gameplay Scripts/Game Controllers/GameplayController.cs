﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameplayController : MonoBehaviour
{
    public static GameplayController instance;

    public GameObject messagePopUp;
    public GameObject upgradesPanel;
    public GameObject inventoryPanel;

    public Sprite[] rewardSprites;

    private bool messageShowing = false;

    void Awake()
    {
        MakeInstance();

        StatsController.ResetQuestStats();
        PhysicsObjectsContainerHolder.physicsObjectContainers.Clear();
    }

    /// <summary>
    /// Initialise controller instance
    /// </summary>
    void MakeInstance()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    /// <summary>
    /// Spawn random item at position
    /// </summary>
    /// <param name="positionToSpawnAt">Position to spawn item at</param>
    public void SpawnRandomItem(Vector3 positionToSpawnAt)
    {
        string[] dropableItems = { Constants.bombItemName, Constants.appleItemName, Constants.cherryItemName, Constants.healthPotionItemName, Constants.staminaPotionItemName, Constants.purpleGemItemName, Constants.blueGemItemName, Constants.silverGemItemName };

        string itemFullPath = Constants.itemsPrefabFolder;
        string itemPath = dropableItems[Random.Range(0, dropableItems.Length)];
        itemFullPath += itemPath;

        GameObject itemPrefab = Resources.Load(itemFullPath) as GameObject;
        Instantiate(itemPrefab, positionToSpawnAt, itemPrefab.transform.rotation);
    }

    /// <summary>
    /// Show message when using an item
    /// </summary>
    /// <param name="item">Item used</param>
    public void ItemUseDescription(GameItem item)
    {
        if (GameItem.CheckIfEmelynItem(item.name) || item.name == Constants.bombItemName)
        {
            ShowMessage(item.name, item.effectDescription, Constants.longMessage, null);
        }
        else
        {
            ShowMessage(item.name, item.effectDescription, Constants.shortMessage, null);
        }
    }

    /// <summary>
    /// Show UI message for item limit reached
    /// </summary>
    public void ItemLimit()
    {
        ShowMessage(Constants.itemLimitTitle, Constants.itemLimitMessage, Constants.shortMessage, null);
    }

    /// <summary>
    /// Show UI message for bag limit reached
    /// </summary>
    public void BagFull()
    {
        ShowMessage(Constants.bagLimitTitle, Constants.bagLimitMessage, Constants.shortMessage, null);
    }

    /// <summary>
    /// Show UI message for upgrade locked
    /// </summary>
    public void UpgradeLocked()
    {
        ShowMessage(Constants.upgradeLockedTitle, Constants.upgradeLockedMessage, Constants.shortMessage, null);
    }

    /// <summary>
    /// Show UI message for purchase failed
    /// </summary>
    public void PurchaseFailed()
    {
        ShowMessage(Constants.purchaseUnavailableTitle, Constants.purchaseUnavailableMessage, Constants.longMessage, null);
    }

    /// <summary>
    /// Show UI message for signpost message
    /// </summary>
    public void ReadSignpost()
    {
        ShowMessage(Constants.mysteriousFruitsTitle, Constants.mysteriousFruitsMessage, Constants.longMessage, null);
    }

    /// <summary>
    /// Play sound and show UI message for achievement unlocked
    /// </summary>
    /// <param name="achievement">Achievement unlocked</param>
    public void UnlockAchievement(Achievement achievement)
    {
        AchievementUnlock();
        ShowMessage(Constants.achivementUnlockedTitle, achievement.description, Constants.shortMessage, null);
    }

    /// <summary>
    /// Play sound and show UI message for quest completed
    /// </summary>
    /// <param name="achievement">Quest completed</param>
    public void CompleteQuest(Quest quest)
    {
        PurchaseUpgrade();
        ShowMessage(Constants.questCompletedTitle, quest.description, Constants.questMessage, quest.reward);
    }

    /// <summary>
    /// Play sound and show UI message for new quest unlocked
    /// </summary>
    /// <param name="achievement">Quest unlocked</param>
    public void NewQuest(Quest quest)
    {
        AchievementUnlock();
        ShowMessage(Constants.questUnlockedTitle, quest.description, Constants.shortMessage, null);
    }

    /// <summary>
    /// Show UI message, if message not already showing
    /// </summary>
    /// <param name="title">Message title</param>
    /// <param name="message">Message description</param>
    /// <param name="messageType">Message type</param>
    /// <param name="reward">Quest reward detail</param>
    public void ShowMessage(string title, string message, int messageType, string reward)
    {
        if (!messageShowing)
        {
            switch (messageType)
            {
                case 1:
                    //Short message
                    messagePopUp.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = title;
                    messagePopUp.transform.GetChild(2).GetComponent<Text>().text = message;

                    messagePopUp.transform.GetChild(3).GetComponent<Text>().text = Constants.empty;
                    messagePopUp.transform.GetChild(4).transform.GetChild(0).GetComponent<Text>().text = Constants.empty;
                    messagePopUp.transform.GetChild(4).transform.GetChild(1).GetComponent<Image>().enabled = false;
                    messagePopUp.transform.GetChild(4).transform.GetChild(1).GetComponent<Image>().enabled = false;
                    messagePopUp.transform.GetChild(4).transform.GetChild(2).GetComponent<Text>().enabled = false;
                    break;
                case 2:
                    //Long message
                    messagePopUp.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = title;
                    messagePopUp.transform.GetChild(3).GetComponent<Text>().text = message;

                    messagePopUp.transform.GetChild(2).GetComponent<Text>().text = Constants.empty;
                    messagePopUp.transform.GetChild(4).transform.GetChild(0).GetComponent<Text>().text = Constants.empty;
                    messagePopUp.transform.GetChild(4).transform.GetChild(1).GetComponent<Image>().enabled = false;
                    messagePopUp.transform.GetChild(4).transform.GetChild(1).GetComponent<Image>().enabled = false;
                    messagePopUp.transform.GetChild(4).transform.GetChild(2).GetComponent<Text>().enabled = false;
                    break;
                case 3:
                    //Quest message
                    messagePopUp.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = title;
                    messagePopUp.transform.GetChild(4).transform.GetChild(0).GetComponent<Text>().text = message;
                    messagePopUp.transform.GetChild(4).transform.GetChild(1).GetComponent<Image>().sprite = GetRewardSprite(reward);
                    messagePopUp.transform.GetChild(4).transform.GetChild(1).GetComponent<Image>().enabled = true;
                    messagePopUp.transform.GetChild(4).transform.GetChild(1).GetComponent<Image>().enabled = true;
                    messagePopUp.transform.GetChild(4).transform.GetChild(2).GetComponent<Text>().enabled = true;

                    messagePopUp.transform.GetChild(2).GetComponent<Text>().text = Constants.empty;
                    messagePopUp.transform.GetChild(3).GetComponent<Text>().text = Constants.empty;
                    break;
            }

            StartCoroutine(FadeInOut());
        }
    }

    /// <summary>
    /// Slide message onto screen for 3 seconds then slide it off
    /// </summary>
    /// <returns>Seconds waited</returns>
    IEnumerator FadeInOut()
    {
        messageShowing = true;
        messagePopUp.GetComponent<Animator>().Play(Constants.messageSlideInState);

        yield return new WaitForSeconds(3f);

        messagePopUp.GetComponent<Animator>().Play(Constants.messageSlideOutState);
        messageShowing = false;
    }

    /// <summary>
    /// Get item reward sprite to display
    /// </summary>
    /// <param name="reward">Reward to get sprite for</param>
    /// <returns>Reward sprite</returns>
    public Sprite GetRewardSprite(string reward)
    {
        if (reward == Constants.silverGemReward)
        {
            return rewardSprites[Constants.silverGemRewardSprite];
        }
        else if (reward == Constants.blueGemReward)
        {
            return rewardSprites[Constants.blueGemRewardSprite];
        }
        else if (reward == Constants.purpleGemReward)
        {
            return rewardSprites[Constants.purpleGemRewardSprite];
        }

        return null;
    }

    /// <summary>
    /// Play open inventory sound
    /// </summary>
    public void OpenInventory()
    {
        if (MusicController.instance != null)
        {
            MusicController.instance.PlayOpenInventorySound();
        }
    }

    /// <summary>
    /// Play open upgrades sound
    /// </summary>
    public void OpenUpgrades()
    {
        if (MusicController.instance != null)
        {
            MusicController.instance.PlayOpenUpgradesSound();
        }
    }

    /// <summary>
    /// Play pick up sound
    /// </summary>
    public void PickUpItem()
    {
        if (MusicController.instance != null)
        {
            MusicController.instance.PlayPickUpSound();
        }
    }

    /// <summary>
    /// Play purchase sound
    /// </summary>
    public void PurchaseUpgrade()
    {
        //Play the purchase sound
        if (MusicController.instance != null)
        {
            MusicController.instance.PlayPurchaseSound();
        }
    }

    /// <summary>
    /// Play lose life sound
    /// </summary>
    public void LoseLife()
    {
        if (MusicController.instance != null)
        {
            MusicController.instance.PlayLoseLifeSound();
        }
    }

    /// <summary>
    /// Play explosion sound
    /// </summary>
    public void Explosion()
    {
        if (MusicController.instance != null)
        {
            MusicController.instance.PlayExplosionSound();
        }
    }

    /// <summary>
    /// Play achievement unlocked sound
    /// </summary>
    public void AchievementUnlock()
    {
        if (MusicController.instance != null)
        {
            MusicController.instance.PlayAchievementSound();
        }
    }

    /// <summary>
    /// Play secret path found sound
    /// </summary>
    public void DiscoverSecretPath()
    { 
        if (MusicController.instance != null)
        {
            MusicController.instance.PlaySecretPathSound();
        }
    }

    /// <summary>
    /// Pause game
    /// </summary>
    public void PauseGame()
    {
        Time.timeScale = 0;
    }

    /// <summary>
    /// Resume game
    /// </summary>
    public void ResumeGame()
    {
        Time.timeScale = 1;
    }
}