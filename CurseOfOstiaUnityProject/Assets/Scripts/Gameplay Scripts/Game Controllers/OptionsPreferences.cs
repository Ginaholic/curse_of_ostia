﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OptionsPreferences : MonoBehaviour
{
    private static string SoundOn = Constants.soundOn;
    private static string SFXVolume = Constants.sfxVolume;
    private static string MusicVolume = Constants.musicVolume;
    
    /// <summary>
    /// Set sound on or off state
    /// </summary>
    /// <param name="state">Int sound state</param>
    public static void SetSoundState(int state)
    {
        PlayerPrefs.SetInt(SoundOn, state);
    }

    /// <summary>
    /// Get sound state
    /// </summary>
    /// <returns>Int sound state</returns>
    public static int GetSoundState()
    {
        return PlayerPrefs.GetInt(SoundOn);
    }

    /// <summary>
    /// Set sfx volume
    /// </summary>
    /// <param name="volume">New volume</param>
    public static void SetSFXVolume(float volume)
    {
        PlayerPrefs.SetFloat(SFXVolume, volume);
    }

    /// <summary>
    /// Get sfx volume
    /// </summary>
    /// <returns>Float sfx volume</returns>
    public static float GetSFXVolume()
    {
        return PlayerPrefs.GetFloat(SFXVolume);
    }

    /// <summary>
    /// Set music volume
    /// </summary>
    /// <param name="volume">New volume</param>
    public static void SetMusicVolume(float volume)
    {
        PlayerPrefs.SetFloat(MusicVolume, volume);
    }

    /// <summary>
    /// Get music volume
    /// </summary>
    /// <returns>Float music volume</returns>
    public static float GetMusicVolume()
    {
        return PlayerPrefs.GetFloat(MusicVolume);
    }
}
