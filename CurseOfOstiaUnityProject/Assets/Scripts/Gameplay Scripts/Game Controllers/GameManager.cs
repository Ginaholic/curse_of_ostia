﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    void Awake()
    {
        MakeSingleton();
    }

    void Start()
    {
        //Initialise variable defaults and achievements
        InitialiseVariables();
        AchievementController.InitialiseAchievements();
    }

    void OnLevelWasLoaded()
    {
        //Play the appropriate music and reset debug variables
        if (SceneManager.GetActiveScene().name == Constants.levelScene)
        {
            MusicController.instance.PlayGameMusic();

            DebugGraphicsController.collidersOn = false;
            DebugGraphicsController.pathsOn = false;
            DebugGraphicsController.nodesOn = false;
        }
        else
        {
            MusicController.instance.PlayMenuMusic();
        }
    }

    /// <summary>
    /// Initialise controller singleton
    /// </summary>
    void MakeSingleton()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    /// <summary>
    /// Set default variables if not already done
    /// </summary>
    void InitialiseVariables()
    {
        if (!PlayerPrefs.HasKey(Constants.gameInitialised))
        {
            OptionsPreferences.SetSoundState(1);
            OptionsPreferences.SetSFXVolume(1);
            OptionsPreferences.SetMusicVolume(1);
            
            PlayerPrefs.SetInt(Constants.gameInitialised, 1);
        }
    }
}
