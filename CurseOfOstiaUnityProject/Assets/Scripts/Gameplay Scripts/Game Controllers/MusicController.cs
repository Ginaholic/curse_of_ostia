﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class MusicController : MonoBehaviour
{
    public static MusicController instance;

    public AudioSource musicAudioSource;
    public AudioClip[] musicAudioClips;
    
    public AudioSource SFXAudioSource;
    public AudioClip[] SFXAudioClips;

    void Awake()
    {
        MakeSingleton();
    }

    void Start()
    {
        PlayMusic();
        SetMusicVolume(OptionsPreferences.GetMusicVolume());
        SetSFXVolume(OptionsPreferences.GetSFXVolume());
    }

    /// <summary>
    /// Initialise controller singleton
    /// </summary>
    void MakeSingleton()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    /// <summary>
    /// Check if sound preference is on or off
    /// </summary>
    /// <returns>Bool play sound</returns>
    public bool CheckWhetherToPlay()
    {
        if (OptionsPreferences.GetSoundState() == 1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// Play music if it's not already playing
    /// </summary>
    public void PlayMusic()
    {
        if (CheckWhetherToPlay())
        {
            if (!musicAudioSource.isPlaying)
            {
                musicAudioSource.Play();
            }
        }
    }

    /// <summary>
    /// Stop the music
    /// </summary>
    public void StopMusic()
    {
        if (musicAudioSource.isPlaying)
        {
            musicAudioSource.Stop();
        }
    }

    /// <summary>
    /// Play the main menu music clip
    /// </summary>
    public void PlayMenuMusic()
    {
        musicAudioSource.clip = musicAudioClips[0];
        PlayMusic();
    }

    /// <summary>
    /// Play game music clip
    /// </summary>
    public void PlayGameMusic()
    {
        musicAudioSource.clip = musicAudioClips[1];
        PlayMusic();
    }

    /// <summary>
    /// Set music volume
    /// </summary>
    /// <param name="volume">New music volume</param>
    public void SetMusicVolume(float volume)
    {
        musicAudioSource.volume = volume;
    }

    /// <summary>
    /// Play sfx sound
    /// </summary>
    public void PlaySound()
    {
        if (CheckWhetherToPlay())
        {
            SFXAudioSource.Play();
        }
    }

    /// <summary>
    /// Play the open inventory clip
    /// </summary>
    public void PlayOpenInventorySound()
    {
        SFXAudioSource.clip = SFXAudioClips[Constants.openInventoryClip];
        PlaySound();
    }

    /// <summary>
    /// Play the open upgrades clip
    /// </summary>
    public void PlayOpenUpgradesSound()
    {
        SFXAudioSource.clip = SFXAudioClips[Constants.openUpgradesClip];
        PlaySound();
    }

    /// <summary>
    /// Play the pick up clip
    /// </summary>
    public void PlayPickUpSound()
    {
        SFXAudioSource.clip = SFXAudioClips[Constants.pickUpClip];
        PlaySound();
    }

    /// <summary>
    /// Play the purchase clip
    /// </summary>
    public void PlayPurchaseSound()
    {
        SFXAudioSource.clip = SFXAudioClips[Constants.purchaseClip];
        PlaySound();
    }

    /// <summary>
    /// Play the lose life clip
    /// </summary>
    public void PlayLoseLifeSound()
    {
        SFXAudioSource.clip = SFXAudioClips[Constants.loseLifeClip];
        PlaySound();
    }

    /// <summary>
    /// Play the explosion clip
    /// </summary>
    public void PlayExplosionSound()
    {
        SFXAudioSource.clip = SFXAudioClips[Constants.explosionClip];
        PlaySound();
    }

    /// <summary>
    /// Play the achievement unlocked clip
    /// </summary>
    public void PlayAchievementSound()
    {
        SFXAudioSource.clip = SFXAudioClips[Constants.achievementClip];
        PlaySound();
    }

    /// <summary>
    /// Play the secret path found upgrades clip
    /// </summary>
    public void PlaySecretPathSound()
    {
        SFXAudioSource.clip = SFXAudioClips[Constants.secretPathClip];
        PlaySound();
    }

    /// <summary>
    /// Set sfx volume
    /// </summary>
    /// <param name="volume">New sfx volume</param>
    public void SetSFXVolume(float volume)
    {
        //Set the sfw volume
        SFXAudioSource.volume = volume;
    }
}
