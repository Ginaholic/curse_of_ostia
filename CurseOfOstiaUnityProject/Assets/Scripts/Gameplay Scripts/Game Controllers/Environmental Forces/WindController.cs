﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindController : MonoBehaviour
{
    public float windOnTime = Constants.windOnTime;
    public float windOffTime = Constants.windOffTime;

    public float timer = 0f; 

    public static Vector2 currentWindForce;
    public GameObject windSystems;

    void Start()
    {
        StartCoroutine(WindCycle());
    }
    
    /// <summary>
    /// Cycle through on/off states of random wind forces recursively
    /// </summary>
    IEnumerator WindCycle()
    {
        TurnOffWind();
        yield return new WaitForSeconds(windOffTime);

        TurnOnWind();
        yield return new WaitForSeconds(windOnTime);

        StartCoroutine(WindCycle());
    }

    /// <summary>
    /// Turn off wind, reset wind force and stop all animations
    /// </summary>
    public void TurnOffWind()
    {
        currentWindForce = new Vector2(0, 0);

        windSystems.transform.GetChild(0).GetComponent<ParticleSystem>().Stop();
        windSystems.transform.GetChild(1).GetComponent<ParticleSystem>().Stop();
        windSystems.transform.GetChild(2).GetComponent<ParticleSystem>().Stop();
        windSystems.transform.GetChild(3).GetComponent<ParticleSystem>().Stop();
    }

    /// <summary>
    /// Turn on wind, select random direction, play it's animation and set wind force
    /// </summary>
    public void TurnOnWind()
    {
        int windIndex = Random.Range(0, 3);
        
        windSystems.transform.GetChild(windIndex).GetComponent<ParticleSystem>().Play();

        switch (windIndex)
        {
            case Constants.windLeftIndex:
                currentWindForce = Constants.windLeftForce;
                break;
            case Constants.windRighttIndex:
                currentWindForce = Constants.windRightForce;
                break;
            case Constants.windUpIndex:
                currentWindForce = Constants.windUpForce;
                break;
            case Constants.windDownIndex:
                currentWindForce = Constants.windDownForce;
                break;
        }
    }        
}
