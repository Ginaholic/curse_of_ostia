﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradesController : MonoBehaviour
{
    public static UpgradesController instance;

    public Toggle arrowButton;
    public Toggle bowButton;

    public GameObject bowTiersPanel;
    public GameObject arrowTiersPanel;

    public GameObject currencyPanel;

    private static int purpleGemsCount;
    private static int blueGemsCount;
    private static int silverGemsCount;

    private Color buttonPressedColour = new Color32(0xE4, 0x7A, 0x7E, 0xFF);
    private Color buttonNormalColour = new Color32(0xA0, 0x96, 0xD1, 0xFF);

    void Awake()
    {
        MakeInstance();
    }

    void Start()
    {
        InitialiseUpgradeTiers();

        //Show default upgrades tab
        bowButton.isOn = true;
        ShowTiers();

        //Set active weapon tiers active in panel
        UpdateActiveTierPanel(ArrowTierController.activeArrowTier.id, Constants.arrowUpgradeTier);
        UpdateActiveTierPanel(BowTierController.activeBowTier.id, Constants.bowUpgradeTier);

        UpdateCurrencyPanel();
    }

    /// <summary>
    /// Initialise controller instance
    /// </summary>
    void MakeInstance()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    /// <summary>
    /// Show upgrade tiers based on which button was pressed
    /// </summary>
    public void ShowTiers()
    {
        if (bowButton.isOn)
        {
            arrowButton.isOn = false;
        }
        else if (arrowButton.isOn)
        {
            bowButton.isOn = false;
        }
    }

    /// <summary>
    /// Activate or purchase the weapon tier clicked on
    /// </summary>
    /// <param name="tierId">Id of weapon tier clicked on</param>
    public void UpgradeButtonClicked(int tierId)
    {
        if (arrowButton.isOn)
        {
            if (arrowTiersPanel.transform.GetChild(tierId).GetChild(3).GetComponent<Toggle>().isOn)
            {
                if (!ArrowTierController.CheckIfTierLocked(tierId))
                {
                    if (ArrowTierController.CheckIfTierPurchased(tierId))
                    {
                        if (!ArrowTierController.CheckIfTierActive(tierId))
                        {
                            ActivateTier(tierId, Constants.arrowUpgradeTier);
                        }
                    }
                    else
                    {
                        PurchaseTier(tierId, Constants.arrowUpgradeTier);
                    }
                }
                else
                {
                    GameplayController.instance.UpgradeLocked();
                }
            }
        }
        else if (bowButton.isOn)
        {
            if (bowTiersPanel.transform.GetChild(tierId).GetChild(3).GetComponent<Toggle>().isOn)
            {
                if (!BowTierController.CheckIfTierLocked(tierId))
                {
                    if (BowTierController.CheckIfTierPurchased(tierId))
                    {
                        if (!BowTierController.CheckIfTierActive(tierId))
                        {
                            ActivateTier(tierId, "bow");
                        }
                    }
                    else
                    {
                        PurchaseTier(tierId, "bow");
                    }
                }
                else
                {
                    GameplayController.instance.UpgradeLocked();
                }
            }
        }
    }

    /// <summary>
    /// Deactivate current weapon tier and activate new tier
    /// </summary>
    /// <param name="tierId">Id of weapon tier to activate</param>
    /// <param name="tierType">Type of weapon tier to activate</param>
    public void ActivateTier(int tierId, string tierType)
    {
        UpdateDeactiveCurrentTier(tierType);

        if (tierType == Constants.arrowUpgradeTier)
        {
            ArrowTier tier = ArrowTierController.GetArrowTier(tierId);
            tier.ActivateArrowTier();
        }
        else if (tierType == Constants.bowUpgradeTier)
        {
            BowTier tier = BowTierController.GetBowTier(tierId);
            tier.ActivateBowTier();
        }

        UpdateActiveTierPanel(tierId, tierType);
    }

    /// <summary>
    /// Update UI to deactivate the current weapon tier
    /// </summary>
    /// <param name="tierType">Type of weapon tier to deactivate</param>
    public void UpdateDeactiveCurrentTier(string tierType)
    {
        if (tierType == Constants.arrowUpgradeTier)
        {
            if (ArrowTierController.activeArrowTier != null)
            {
                arrowTiersPanel.transform.GetChild(ArrowTierController.activeArrowTier.id).GetChild(3).GetComponent<Image>().color = buttonNormalColour;
                ArrowTierController.activeArrowTier.DeactivateArrowTier();
            }
        }
        else if (tierType == Constants.bowUpgradeTier)
        {
            if (BowTierController.activeBowTier != null)
            {
                bowTiersPanel.transform.GetChild(BowTierController.activeBowTier.id).GetChild(3).GetComponent<Image>().color = buttonNormalColour;
                BowTierController.activeBowTier.DeactivateBowTier();
            }
        }
    }

    /// <summary>
    /// Update UI to activate the specified weapon tier
    /// </summary>
    /// <param name="tierId">Id of weapon tier to activate</param>
    /// <param name="tierType">Type of weapon tier to activate</param>
    public void UpdateActiveTierPanel(int tierId, string tierType)
    {
        if (tierType == Constants.arrowUpgradeTier)
        {
            if (tierId != 0)
            {
                StatsController.ArrowUpgraded();
            }
            
            arrowTiersPanel.transform.GetChild(tierId).GetChild(3).GetComponent<Image>().color = buttonPressedColour;
        }
        else if (tierType == Constants.bowUpgradeTier)
        {
            if (tierId != 0)
            {
                StatsController.BowUpgraded();
            }
            
            bowTiersPanel.transform.GetChild(tierId).GetChild(3).GetComponent<Image>().color = buttonPressedColour;
        }
    }

    /// <summary>
    /// Purchase weapon tier if enough currency
    /// </summary>
    /// <param name="tierId">Id of weapon tier to purchase</param>
    /// <param name="tierType">Type of weapon tier to purchase</param>
    public void PurchaseTier(int tierId, string tierType)
    {
        if (tierId == 1)
        {
            //Tier 1 cost is 2 blue gems
            if (blueGemsCount >= 2)
            {
                blueGemsCount = blueGemsCount - 2;
                UpdateCurrencyPanel();
                UpdatePurchasedTierPanel(tierId, tierType);
            }
            else
            {
                GameplayController.instance.PurchaseFailed();
            }
        }
        else if (tierId == 2)
        {
            //Tier 2 cost is 1 purple gem and 3 silver gems
            if (purpleGemsCount >= 1 && silverGemsCount >= 3)
            {
                purpleGemsCount = purpleGemsCount - 1;
                silverGemsCount = silverGemsCount - 3;
                UpdateCurrencyPanel();
                UpdatePurchasedTierPanel(tierId, tierType);
            }
            else
            {
                GameplayController.instance.PurchaseFailed();
            }
        }
    }

    /// <summary>
    /// Update UI panel and stats after weapon tier purchase
    /// </summary>
    /// <param name="tierId">Id of weapon tier purchased</param>
    /// <param name="tierType">Type of weapon tier purchased</param>
    public void UpdatePurchasedTierPanel(int tierId, string tierType)
    {
        if (tierType == Constants.arrowUpgradeTier)
        {
            ArrowTier tier = ArrowTierController.GetArrowTier(tierId);
            tier.PurchaseArrowTier();
            
            StatsController.IncreaseArrowUpgradePurchasedCount();
            
            arrowTiersPanel.transform.GetChild(tierId).GetChild(2).GetChild(0).GetComponent<Text>().enabled = true;
            arrowTiersPanel.transform.GetChild(tierId).GetChild(2).GetChild(1).gameObject.SetActive(false);

            arrowTiersPanel.transform.GetChild(tierId).GetChild(3).GetChild(1).GetComponent<Text>().enabled = false;
            arrowTiersPanel.transform.GetChild(tierId).GetChild(3).GetChild(0).GetComponent<Text>().enabled = true;
        }
        else if (tierType == Constants.bowUpgradeTier)
        {
            BowTier tier = BowTierController.GetBowTier(tierId);
            tier.PurchaseBowTier();
            
            StatsController.IncreaseBowUpgradePurchasedCount();
            
            bowTiersPanel.transform.GetChild(tierId).GetChild(2).GetChild(0).GetComponent<Text>().enabled = true;
            bowTiersPanel.transform.GetChild(tierId).GetChild(2).GetChild(1).gameObject.SetActive(false);

            bowTiersPanel.transform.GetChild(tierId).GetChild(3).GetChild(1).GetComponent<Text>().enabled = false;
            bowTiersPanel.transform.GetChild(tierId).GetChild(3).GetChild(0).GetComponent<Text>().enabled = true;
        }
    }

    /// <summary>
    /// Unlock weapon tier
    /// </summary>
    /// <param name="tierId">Id of weapon tier to unlock</param>
    /// <param name="tierType">Type of weapon tier to unlock</param>
    public void UnlockTier(int tierId, string tierType)
    {
        UpdateUnlockedTierPanel(tierId, tierType);
    }

    /// <summary>
    /// Update UI panel and stats after weapon tier unlock
    /// </summary>
    /// <param name="tierId">Id of weapon tier unlocked</param>
    /// <param name="tierType">Type of weapon tier unlocked</param>
    public void UpdateUnlockedTierPanel(int tierId, string tierType)
    {
        if (tierType == Constants.arrowUpgradeTier)
        {
            ArrowTier tier = ArrowTierController.GetArrowTier(tierId);
            tier.UnlockArrowTier();
            
            StatsController.IncreaseArrowUpgradeUnlockedCount();
            
            arrowTiersPanel.transform.GetChild(tierId).GetChild(3).GetChild(2).GetComponent<Image>().enabled = false;
            arrowTiersPanel.transform.GetChild(tierId).GetChild(3).GetChild(1).GetComponent<Text>().enabled = true;
        }
        else if (tierType == Constants.bowUpgradeTier)
        {
            BowTier tier = BowTierController.GetBowTier(tierId);
            tier.UnlockBowTier();
            
            StatsController.IncreaseBowUpgradeUnlockedCount();
        
            bowTiersPanel.transform.GetChild(tierId).GetChild(3).GetChild(2).GetComponent<Image>().enabled = false;
            bowTiersPanel.transform.GetChild(tierId).GetChild(3).GetChild(1).GetComponent<Text>().enabled = true;
        }
    }

    /// <summary>
    /// Update UI panel for currency display
    /// </summary>
    public void UpdateCurrencyPanel()
    {
        currencyPanel.transform.GetChild(0).GetChild(1).GetComponent<Text>().text = purpleGemsCount.ToString();
        currencyPanel.transform.GetChild(1).GetChild(1).GetComponent<Text>().text = blueGemsCount.ToString();
        currencyPanel.transform.GetChild(2).GetChild(1).GetComponent<Text>().text = silverGemsCount.ToString();
    }

    /// <summary>
    /// Increase currency amount by 1
    /// </summary>
    /// <param name="currencyType">Currency type to increase</param>
    public void GainCurrency(string currencyType)
    {
        if (currencyType == Constants.purpleGemReward)
        {
            purpleGemsCount++;
        }
        else if (currencyType == Constants.blueGemReward)
        {
            blueGemsCount++;
        }
        else if (currencyType == Constants.silverGemReward)
        {
            silverGemsCount++;
        }

        UpdateCurrencyPanel();
    }

    /// <summary>
    /// Initialise all currency to 0
    /// </summary>
    public static void InitialiseCurrency()
    {
        purpleGemsCount = 0;
        blueGemsCount = 0;
        silverGemsCount = 0;
    }

    /// <summary>
    /// Initialise upgrade tiers, currency and activate default (first) weapon tiers
    /// </summary>
    public static void InitialiseUpgradeTiers()
    {
        BowTierController.InitialiseBowTiers();
        ArrowTierController.InitialiseArrowTiers();
        InitialiseCurrency();
        
        BowTierController.GetBowTier(0).ActivateBowTier();
        ArrowTierController.GetArrowTier(0).ActivateArrowTier();
    }
}
