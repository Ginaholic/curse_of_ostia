﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BowTier
{
    public int id;
    public int maxRange;
    public bool locked;
    public bool bought;
    public bool active;

    public BowTier(int _id, int _maxRange, bool _locked, bool _bought, bool _active)
    {
        id = _id;
        maxRange = _maxRange;
        locked = _locked;
        bought = _bought;
        active = _active;
    }

    /// <summary>
    /// Unlock bow tier, set locked to false
    /// </summary>
    public void UnlockBowTier()
    {
        locked = false;
    }

    /// <summary>
    /// Purchase bow tier, set bought to true
    /// </summary>
    public void PurchaseBowTier()
    {
        bought = true;
    }

    /// <summary>
    /// Activeate bow tier, set active to true and update active bow tier
    /// </summary>
    public void ActivateBowTier()
    {
        active = true;
        BowTierController.activeBowTier = this;
    }

    /// <summary>
    /// Deactivate bow tier, set active to false
    /// </summary>
    public void DeactivateBowTier()
    {
        active = false;
    }
}
