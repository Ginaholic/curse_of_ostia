﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BowTierController : MonoBehaviour
{
    public static BowTierController instance;
    
    public static BowTier activeBowTier;
    public static List<BowTier> bowTiers = new List<BowTier>();

    void Awake()
    {
        MakeInstance();
    }

    /// <summary>
    /// Initialise controller instance
    /// </summary>
    void MakeInstance()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    /// <summary>
    /// Check if weapon tier is unlocked
    /// </summary>
    /// <param name="tierId">Id of weapon tier to check</param>
    /// <returns>Bool tier unlocked</returns>
    public static bool CheckIfTierLocked(int tierId)
    {
        BowTier result = bowTiers.Find(x => x.id == tierId);
        return result.locked;
    }

    /// <summary>
    /// Check if weapon tier is purchased
    /// </summary>
    /// <param name="tierId">Id of weapon tier to check</param>
    /// <returns>Bool tier purchased</returns>
    public static bool CheckIfTierPurchased(int tierId)
    {
        BowTier result = bowTiers.Find(x => x.id == tierId);
        return result.bought;
    }

    /// <summary>
    /// Check if weapon tier is active
    /// </summary>
    /// <param name="tierId">Id of weapon tier to check</param>
    /// <returns>Bool tier active</returns>
    public static bool CheckIfTierActive(int tierId)
    {
        if (tierId == activeBowTier.id)
        {
            return true;
        }

        return false;
    }

    /// <summary>
    /// Get bow tier from list
    /// </summary>
    /// <param name="tierId">Id of bow tier to get</param>
    /// <returns>Bow tier</returns>
    public static BowTier GetBowTier(int tierId)
    {
        //Look for tier in lists, return it
        BowTier result = bowTiers.Find(x => x.id == tierId);
        return result;
    }

    /// <summary>
    /// Initialise all bow tiers and add to list
    /// </summary>
    public static void InitialiseBowTiers()
    {
        bowTiers.Add(new BowTier(Constants.upgradeTier0, 3, false, true, false));
        bowTiers.Add(new BowTier(Constants.upgradeTier1, 7, true, false, false));
        bowTiers.Add(new BowTier(Constants.upgradeTier2, 10, true, false, false));
    }
}
