﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowTier
{
    public int id;
    public int maxDamage;
    public bool locked;
    public bool bought;
    public bool active;

    public ArrowTier(int _id, int _maxDamage, bool _locked, bool _bought, bool _active)
    {
        id = _id;
        maxDamage = _maxDamage;
        locked = _locked;
        bought = _bought;
        active = _active;
    }

    /// <summary>
    /// Unlock arrow tier, set locked to false
    /// </summary>
    public void UnlockArrowTier()
    {
        locked = false;
    }

    /// <summary>
    /// Purchase arrow tier, set bought to true
    /// </summary>
    public void PurchaseArrowTier()
    {
        bought = true;
    }

    /// <summary>
    /// Activeate arrow tier, set active to true and update active bow tier
    /// </summary>
    public void ActivateArrowTier()
    {
        active = true;
        ArrowTierController.activeArrowTier = this;
    }

    /// <summary>
    /// Deactivate arrow tier, set active to false
    /// </summary>
    public void DeactivateArrowTier()
    {
        active = false;
    }
}
