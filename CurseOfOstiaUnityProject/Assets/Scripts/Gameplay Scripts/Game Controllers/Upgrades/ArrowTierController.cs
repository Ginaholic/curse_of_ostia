﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowTierController : MonoBehaviour
{
    public static ArrowTierController instance;

    public static ArrowTier activeArrowTier;
    public static List<ArrowTier> arrowTiers = new List<ArrowTier>();

    void Awake()
    {
        MakeInstance();
    }

    /// <summary>
    /// Initialise controller instance
    /// </summary>
    void MakeInstance()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    /// <summary>
    /// Check if weapon tier is unlocked
    /// </summary>
    /// <param name="tierId">Id of weapon tier to check</param>
    /// <returns>Bool tier unlocked</returns>
    public static bool CheckIfTierLocked(int tierId)
    {
        ArrowTier result = arrowTiers.Find(x => x.id == tierId);
        return result.locked;
    }

    /// <summary>
    /// Check if weapon tier is purchased
    /// </summary>
    /// <param name="tierId">Id of weapon tier to check</param>
    /// <returns>Bool tier purchased</returns>
    public static bool CheckIfTierPurchased(int tierId)
    {
        ArrowTier result = arrowTiers.Find(x => x.id == tierId);
        return result.bought;
    }

    /// <summary>
    /// Check if weapon tier is active
    /// </summary>
    /// <param name="tierId">Id of weapon tier to check</param>
    /// <returns>Bool tier active</returns>
    public static bool CheckIfTierActive(int tierId)
    {
        if (tierId == activeArrowTier.id)
        {
            return true;
        }

        return false;
    }

    /// <summary>
    /// Get arrow tier from list
    /// </summary>
    /// <param name="tierId">Id of arrow tier to get</param>
    /// <returns>Arrow tier</returns>
    public static ArrowTier GetArrowTier(int tierId)
    {
        ArrowTier result = arrowTiers.Find(x => x.id == tierId);
        return result;
    }

    /// <summary>
    /// Initialise all arrow tiers and add to list
    /// </summary>
    public static void InitialiseArrowTiers()
    {
        arrowTiers.Add(new ArrowTier(Constants.upgradeTier0, 25, false, true, false));
        arrowTiers.Add(new ArrowTier(Constants.upgradeTier1, 20, true, false, false));
        arrowTiers.Add(new ArrowTier(Constants.upgradeTier2, 15, true, false, false));
    }
}
