﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugGraphicsController : MonoBehaviour
{
    public Toggle collidersOnButton;
    public Toggle collidersOffButton;
    public Toggle pathsOnButton;
    public Toggle pathsOffButton;
    public Toggle nodesOnButton;
    public Toggle nodesOffButton;

    public GameObject nodeCollection;

    public static bool collidersOn;
    public static bool pathsOn;
    public static bool nodesOn;

    void Start()
    {
        //All debug graphics will default to off
        collidersOnButton.isOn = false;
        collidersOffButton.isOn = true;
        collidersOn = false;

        pathsOnButton.isOn = false;
        pathsOffButton.isOn = true;
        pathsOn = false;

        nodesOnButton.isOn = false;
        nodesOffButton.isOn = true;
        nodeCollection.SetActive(false);
        nodesOn = false;
    }

    /// <summary>
    /// Turn off collider debug graphics
    /// </summary>
    public void CollidersOff()
    {
        if (collidersOffButton.isOn)
        {
            collidersOnButton.isOn = false;
            collidersOffButton.isOn = true;

            collidersOn = false;
        }
    }

    /// <summary>
    /// Turn on collider debug graphics
    /// </summary>
    public void CollidersOn()
    {
        if (collidersOnButton.isOn)
        {
            collidersOnButton.isOn = true;
            collidersOffButton.isOn = false;

            collidersOn = true;
        }
    }

    /// <summary>
    /// Turn off path debug graphics
    /// </summary>
    public void PathsOff()
    {
        if (pathsOffButton.isOn)
        {
            pathsOnButton.isOn = false;
            pathsOffButton.isOn = true;

            pathsOn = false;
        }
    }

    /// <summary>
    /// Turn on path debug graphics
    /// </summary>
    public void PathsOn()
    {
        if (pathsOnButton.isOn)
        {
            pathsOnButton.isOn = true;
            pathsOffButton.isOn = false;

            pathsOn = true;
        }
    }

    /// <summary>
    /// Turn off node debug graphics
    /// </summary>
    public void NodesOff()
    {
        if (nodesOffButton.isOn)
        {
            nodesOnButton.isOn = false;
            nodesOffButton.isOn = true;

            nodesOn = false;
            nodeCollection.SetActive(false);
        }
    }

    /// <summary>
    /// Turn on node debug graphics
    /// </summary>
    public void NodesOn()
    {
        if (nodesOnButton.isOn)
        {
            nodesOnButton.isOn = true;
            nodesOffButton.isOn = false;

            nodesOn = true;
            nodeCollection.SetActive(true);
        }
    }
}
