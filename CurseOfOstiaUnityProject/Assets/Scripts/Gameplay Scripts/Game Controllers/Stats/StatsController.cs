﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatsController : MonoBehaviour
{    
    /// <summary>
    /// Reset all stat counts to 0
    /// </summary>
    public static void ResetQuestStats()
    {
        StatsTracker.SetMolemenKillCount(0);
        StatsTracker.SetTreantKillCount(0);
        StatsTracker.SetEmelynItemCount(0);
        StatsTracker.SetBowUpgradePurchasedCount(0); 
        StatsTracker.SetBowUpgradeUnlockedCount(0);
        StatsTracker.SetArrowUpgradePurchasedCount(0);
        StatsTracker.SetArrowUpgradeUnlockedCount(0);
    }

    /// <summary>
    /// If emelyn not already found, update stats and quest
    /// </summary>
    public static void EmelynFound()
    {
        if (!StatsTracker.GetEmelynFound())
        {
            StatsTracker.SetEmelynFound(true);
            
            if (QuestController.CheckIfQuestActive(Constants.findEmelynQuestIndex))
            {
                Quest quest = QuestController.GetQuest(Constants.findEmelynQuestIndex);
                if (quest != null)
                {
                    quest.CompleteQuest();
                }
            }
        }
    }

    /// <summary>
    /// If bow not already upgraded, update stats and quest
    /// </summary>
    public static void BowUpgraded()
    {
        if (!StatsTracker.GetBowUpgraded())
        {
            StatsTracker.SetBowUpgraded(true);
            
            if (QuestController.CheckIfQuestActive(Constants.upgradeBowQuestIndex))
            {
                Quest quest = QuestController.GetQuest(Constants.upgradeBowQuestIndex);
                if (quest != null)
                {
                    quest.CompleteQuest();
                }
            }
        }
    }

    /// <summary>
    /// If arrow not already upgraded, update stats and quest
    /// </summary>
    public static void ArrowUpgraded()
    {
        if (!StatsTracker.GetArrowUpgraded())
        {
            StatsTracker.SetBowUpgraded(true);
            
            if (QuestController.CheckIfQuestActive(Constants.upgradeArrowQuestIndex))
            {
                Quest quest = QuestController.GetQuest(Constants.upgradeArrowQuestIndex);
                if (quest != null)
                {
                    quest.CompleteQuest();
                }
            }
        }
    }

    /// <summary>
    /// Update stats and quest
    /// </summary>
    public static void IncreaseMolemenKillCount()
    {
        int currentCount = StatsTracker.GetMolemenKillCount();
        StatsTracker.SetMolemenKillCount(currentCount + 1);
        
        if (QuestController.CheckIfQuestActive(Constants.killMolemenQuestIndex))
        {
            Quest quest = QuestController.GetQuest(Constants.killMolemenQuestIndex);
            if (quest != null)
            {
                quest.UpdateQuestProgess();
            }
        }
        else if (!QuestController.CheckIfQuestComplete(Constants.killMolemenQuestIndex))
        {
            Quest quest = QuestController.GetQuest(Constants.killMolemenQuestIndex);
            if (quest != null)
            {
                quest.ActivateQuest();
                quest.UpdateQuestProgess();
            }
        }
    }

    /// <summary>
    /// Update stats and quest
    /// </summary>
    public static void IncreaseTreantKillCount()
    {
        int currentCount = StatsTracker.GetTreantKillCount();
        StatsTracker.SetTreantKillCount(currentCount + 1);
        
        if (QuestController.CheckIfQuestActive(Constants.killTreantsQuestIndex))
        {
            Quest quest = QuestController.GetQuest(Constants.killTreantsQuestIndex);
            if (quest != null)
            {
                quest.UpdateQuestProgess();
            }
        }
        else if (!QuestController.CheckIfQuestComplete(Constants.killTreantsQuestIndex))
        {
            Quest quest = QuestController.GetQuest(Constants.killTreantsQuestIndex);
            if (quest != null)
            {
                quest.ActivateQuest();
                quest.UpdateQuestProgess();
            }
        }
    }

    /// <summary>
    /// Update stats and achievement
    /// </summary>
    public static void IncreaseEmelynItemCount()
    {
        int currentCount = StatsTracker.GetEmelynItemCount();
        StatsTracker.SetEmelynItemCount(currentCount + 1);
        
        if (StatsTracker.GetEmelynItemCount() == Constants.emylynItemCount)
        {
            if (!AchievementController.CheckIfAchievementUnlocked(Constants.emelynsItemsAchievementIndex))
            {
                Achievement achievement = AchievementController.GetAchievement(Constants.emelynsItemsAchievementIndex);
                if (achievement != null)
                {
                    achievement.UnlockAchievement();
                    StatsTracker.SetAchievement6UnlockDate(System.DateTime.Now.ToString(Constants.achievementDateFormat));
                }
            }
        }
    }

    /// <summary>
    /// Update stats and achievement
    /// </summary>
    public static void IncreaseSecretPathFoundCount()
    {
        int currentCount = StatsTracker.GetSecretPathFoundCount();
        StatsTracker.SetSecretPathFoundCount(currentCount + 1);
        
        if (StatsTracker.GetSecretPathFoundCount() == Constants.secretPathsCount)
        {
            if (!AchievementController.CheckIfAchievementUnlocked(Constants.secretPathsAchievementIndex))
            {
                Achievement achievement = AchievementController.GetAchievement(Constants.secretPathsAchievementIndex);
                if (achievement != null)
                {
                    achievement.UnlockAchievement();
                    StatsTracker.SetAchievement5UnlockDate(System.DateTime.Now.ToString(Constants.achievementDateFormat));
                }
            }
        }
    }

    /// <summary>
    /// Update stats and achievement
    /// </summary>
    public static void IncreaseBowUpgradePurchasedCount()
    {
        int currentCount = StatsTracker.GetBowUpgradePurchasedCount();
        StatsTracker.SetBowUpgradePurchasedCount(currentCount + 1);
        
        if (StatsTracker.GetBowUpgradePurchasedCount() == Constants.bowUpgradesCount)
        {
            if (!AchievementController.CheckIfAchievementUnlocked(Constants.purchaseBowUpgradesAchievementIndex))
            {
                Achievement achievement = AchievementController.GetAchievement(Constants.purchaseBowUpgradesAchievementIndex);
                if (achievement != null)
                {
                    achievement.UnlockAchievement();
                    StatsTracker.SetAchievement3UnlockDate(System.DateTime.Now.ToString(Constants.achievementDateFormat));
                }
            }
        }
    }

    /// <summary>
    /// Update stats and achievement
    /// </summary>
    public static void IncreaseArrowUpgradePurchasedCount()
    {
        int currentCount = StatsTracker.GetArrowUpgradePurchasedCount();
        StatsTracker.SetArrowUpgradePurchasedCount(currentCount + 1);
        
        if (StatsTracker.GetArrowUpgradePurchasedCount() == Constants.arrowUpgradesCount)
        {
            if (!AchievementController.CheckIfAchievementUnlocked(Constants.purchaseArrowUpgradesAchievementIndex))
            {
                Achievement achievement = AchievementController.GetAchievement(Constants.purchaseArrowUpgradesAchievementIndex);
                if (achievement != null)
                {
                    achievement.UnlockAchievement();
                    StatsTracker.SetAchievement4UnlockDate(System.DateTime.Now.ToString(Constants.achievementDateFormat));
                }
            }
        }
    }

    /// <summary>
    /// Update stats, achievement and quest
    /// </summary>
    public static void IncreaseBowUpgradeUnlockedCount()
    {
        int currentCount = StatsTracker.GetBowUpgradeUnlockedCount();
        StatsTracker.SetBowUpgradeUnlockedCount(currentCount + 1);
        
        if (StatsTracker.GetBowUpgradeUnlockedCount() == Constants.bowUpgradesCount)
        {
            if (!AchievementController.CheckIfAchievementUnlocked(Constants.unlockBowUpgradesAchievementIndex))
            {
                Achievement achievement = AchievementController.GetAchievement(Constants.unlockBowUpgradesAchievementIndex);
                if (achievement != null)
                {
                    achievement.UnlockAchievement();
                    StatsTracker.SetAchievement1UnlockDate(System.DateTime.Now.ToString(Constants.achievementDateFormat));
                }
            }
        }
        else if (StatsTracker.GetBowUpgradeUnlockedCount() == 1)
        {
            if (!QuestController.CheckIfQuestActive(Constants.upgradeBowQuestIndex))
            {
                Quest quest = QuestController.GetQuest(Constants.upgradeBowQuestIndex);
                if (quest != null)
                {
                    quest.ActivateQuest();
                }
            }
        }
    }

    /// <summary>
    /// Update stats, achievement and quest
    /// </summary>
    public static void IncreaseArrowUpgradeUnlockedCount()
    {
        int currentCount = StatsTracker.GetArrowUpgradeUnlockedCount();
        StatsTracker.SetArrowUpgradeUnlockedCount(currentCount + 1);
        
        if (StatsTracker.GetArrowUpgradeUnlockedCount() == Constants.arrowUpgradesCount)
        {
            if (!AchievementController.CheckIfAchievementUnlocked(Constants.unlockArrowUpgradesAchievementIndex))
            {
                Achievement achievement = AchievementController.GetAchievement(Constants.unlockArrowUpgradesAchievementIndex);
                if (achievement != null)
                {
                    achievement.UnlockAchievement();
                    StatsTracker.SetAchievement2UnlockDate(System.DateTime.Now.ToString(Constants.achievementDateFormat));
                }
            }
        }
        else if (StatsTracker.GetArrowUpgradeUnlockedCount() == 1)
        {
            if (!QuestController.CheckIfQuestActive(Constants.upgradeArrowQuestIndex))
            {
                Quest quest = QuestController.GetQuest(Constants.upgradeArrowQuestIndex);
                if (quest != null)
                {
                    quest.ActivateQuest();
                }
            }
        }
    }
}
