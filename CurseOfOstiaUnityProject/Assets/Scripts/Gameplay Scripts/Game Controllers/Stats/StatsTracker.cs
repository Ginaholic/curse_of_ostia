﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatsTracker : MonoBehaviour
{
    /// <summary>
    /// Set emelyn found bool as int
    /// </summary>
    /// <param name="state">Bool emelyn found</param>
    public static void SetEmelynFound(bool state)
    {
        PlayerPrefs.SetInt(Constants.emelynFound, state == true ? 1 : 0);
    }

    /// <summary>
    /// Get emelyn found state as bool
    /// </summary>
    /// <returns>Bool emelyn found</returns>
    public static bool GetEmelynFound()
    {
        return PlayerPrefs.GetInt(Constants.emelynFound) == 1 ? true : false;
    }

    /// <summary>
    /// Set bow upgraded as int
    /// </summary>
    /// <param name="state">Bool bow upgraded</param>
    public static void SetBowUpgraded(bool state)
    {
        PlayerPrefs.SetInt(Constants.bowUpgraded, state == true? 1: 0);
    }

    /// <summary>
    /// Get bow upgraded state as bool
    /// </summary>
    /// <returns>Bool bow upgraded</returns>
    public static bool GetBowUpgraded()
    {
        return PlayerPrefs.GetInt(Constants.bowUpgraded) == 1 ? true : false;
    }

    /// <summary>
    /// Set arrow upgrades as int
    /// </summary>
    /// <param name="state">Bool arrow upgraded</param>
    public static void SetArrowUpgraded(bool state)
    {
        PlayerPrefs.SetInt(Constants.arrowUpgraded, state == true ? 1 : 0);
    }

    /// <summary>
    /// Get arrow upgraded state as bool
    /// </summary>
    /// <returns>Bool arrow upgraded</returns>
    public static bool GetArrowUpgraded()
    {
        //Get the state as a bool
        return PlayerPrefs.GetInt(Constants.arrowUpgraded) == 1 ? true : false;
    }
    
    /// <summary>
    /// Set molemen kill count
    /// </summary>
    /// <param name="killCount">New kill count</param>
    public static void SetMolemenKillCount(int killCount)
    {
        PlayerPrefs.SetInt(Constants.molemenKillCount, killCount);
    }

    /// <summary>
    /// Get molemen kill count
    /// </summary>
    /// <returns>Molemen kill count</returns>
    public static int GetMolemenKillCount()
    {
        return PlayerPrefs.GetInt(Constants.molemenKillCount);
    }

    /// <summary>
    /// Set treant kill count
    /// </summary>
    /// <param name="killCount">New kill count</param>
    public static void SetTreantKillCount(int killCount)
    {
        PlayerPrefs.SetInt(Constants.treantKillCount, killCount);
    }

    /// <summary>
    /// Get treant kill count
    /// </summary>
    /// <returns>Treant kill count</returns>
    public static int GetTreantKillCount()
    {
        return PlayerPrefs.GetInt(Constants.treantKillCount);
    }

    /// <summary>
    /// Set emelyn items found count
    /// </summary>
    /// <param name="killCount">New item found count</param>
    public static void SetEmelynItemCount(int itemCount)
    {
        PlayerPrefs.SetInt(Constants.emelynItemCount, itemCount);
    }

    /// <summary>
    /// Get emelyn items found count
    /// </summary>
    /// <returns>Items found count</returns>
    public static int GetEmelynItemCount()
    {
        return PlayerPrefs.GetInt(Constants.emelynItemCount);
    }

    /// <summary>
    /// Set secret paths found count
    /// </summary>
    /// <param name="killCount">New paths found count</param>
    public static void SetSecretPathFoundCount(int pathFoundCount)
    {
        PlayerPrefs.SetInt(Constants.emelynItemCount, pathFoundCount);
    }

    /// <summary>
    /// Get secret paths found count
    /// </summary>
    /// <returns>Paths found count</returns>
    public static int GetSecretPathFoundCount()
    {
        return PlayerPrefs.GetInt(Constants.emelynItemCount);
    }

    /// <summary>
    /// Set bow upgrades purchased count
    /// </summary>
    /// <param name="bowPurchasedCount">New purchased count</param>
    public static void SetBowUpgradePurchasedCount(int bowPurchasedCount)
    {
        PlayerPrefs.SetInt(Constants.bowUpgradePurchasedCount, bowPurchasedCount);
    }

    /// <summary>
    /// Get bow upgrades purchased count
    /// </summary>
    /// <returns>Purchased count</returns>
    public static int GetBowUpgradePurchasedCount()
    {
        return PlayerPrefs.GetInt(Constants.bowUpgradePurchasedCount);
    }

    /// <summary>
    /// Set arrow upgrades purchased count
    /// </summary>
    /// <param name="bowPurchasedCount">New purchased count</param>
    public static void SetArrowUpgradePurchasedCount(int arrowPurchasedCount)
    {
        PlayerPrefs.SetInt(Constants.arrowUpgradePurchasedCount, arrowPurchasedCount);
    }

    /// <summary>
    /// Get arrow upgrades purchased count
    /// </summary>
    /// <returns>Purchased count</returns>
    public static int GetArrowUpgradePurchasedCount()
    {
        return PlayerPrefs.GetInt(Constants.arrowUpgradePurchasedCount);
    }

    /// <summary>
    /// Set bow upgrades unlocked count
    /// </summary>
    /// <param name="bowPurchasedCount">New unlocked count</param>
    public static void SetBowUpgradeUnlockedCount(int bowUnlockedCount)
    {
        PlayerPrefs.SetInt(Constants.bowUpgradeUnlockedCount, bowUnlockedCount);
    }

    /// <summary>
    /// Get bow upgrades unlocked count
    /// </summary>
    /// <returns>Unlocked count</returns>
    public static int GetBowUpgradeUnlockedCount()
    {
        return PlayerPrefs.GetInt(Constants.bowUpgradeUnlockedCount);
    }

    /// <summary>
    /// Set arrow upgrades unlocked count
    /// </summary>
    /// <param name="bowPurchasedCount">New unlocked count</param>
    public static void SetArrowUpgradeUnlockedCount(int arrowUnlockedCount)
    {
        PlayerPrefs.SetInt(Constants.arrowUpgradeUnlockedCount, arrowUnlockedCount);
    }

    /// <summary>
    /// Get arrow upgrades unlocked count
    /// </summary>
    /// <returns>Unlocked count</returns>
    public static int GetArrowUpgradeUnlockedCount()
    {
        return PlayerPrefs.GetInt(Constants.arrowUpgradeUnlockedCount);
    }

    /// <summary>
    /// Set achievement 1 unlock date
    /// </summary>
    /// <param name="unlockDate">Unlock date</param>
    public static void SetAchievement1UnlockDate(string unlockDate)
    {
        PlayerPrefs.SetString(Constants.achievement1UnlockDate, unlockDate);
    }

    /// <summary>
    /// Get achievement 1 unlock date
    /// </summary>
    /// <returns>Unlock date or null</returns>
    public static string GetAchievement1UnlockDate()
    {
        return PlayerPrefs.GetString(Constants.achievement1UnlockDate, null);
    }

    /// <summary>
    /// Set achievement 2 unlock date
    /// </summary>
    /// <param name="unlockDate">Unlock date</param>
    public static void SetAchievement2UnlockDate(string unlockDate)
    {
        PlayerPrefs.SetString(Constants.achievement2UnlockDate, unlockDate);
    }

    /// <summary>
    /// Get achievement 2 unlock date
    /// </summary>
    /// <returns>Unlock date or null</returns>
    public static string GetAchievement2UnlockDate()
    {
        return PlayerPrefs.GetString(Constants.achievement2UnlockDate, null);
    }

    /// <summary>
    /// Set achievement 3 unlock date
    /// </summary>
    /// <param name="unlockDate">Unlock date</param>
    public static void SetAchievement3UnlockDate(string unlockDate)
    {
        PlayerPrefs.SetString(Constants.achievement3UnlockDate, unlockDate);
    }

    /// <summary>
    /// Get achievement 3 unlock date
    /// </summary>
    /// <returns>Unlock date or null</returns>
    public static string GetAchievement3UnlockDate()
    {
        return PlayerPrefs.GetString(Constants.achievement3UnlockDate, null);
    }

    /// <summary>
    /// Set achievement 4 unlock date
    /// </summary>
    /// <param name="unlockDate">Unlock date</param>
    public static void SetAchievement4UnlockDate(string unlockDate)
    {
        PlayerPrefs.SetString(Constants.achievement4UnlockDate, unlockDate);
    }

    /// <summary>
    /// Get achievement 4 unlock date
    /// </summary>
    /// <returns>Unlock date or null</returns>
    public static string GetAchievement4UnlockDate()
    {
        return PlayerPrefs.GetString(Constants.achievement4UnlockDate, null);
    }

    /// <summary>
    /// Set achievement 5 unlock date
    /// </summary>
    /// <param name="unlockDate">Unlock date</param>
    public static void SetAchievement5UnlockDate(string unlockDate)
    {
        PlayerPrefs.SetString(Constants.achievement5UnlockDate, unlockDate);
    }

    /// <summary>
    /// Get achievement 5 unlock date
    /// </summary>
    /// <returns>Unlock date or null</returns>
    public static string GetAchievement5UnlockDate()
    {
        return PlayerPrefs.GetString(Constants.achievement5UnlockDate, null);
    }

    /// <summary>
    /// Set achievement 6 unlock date
    /// </summary>
    /// <param name="unlockDate">Unlock date</param>
    public static void SetAchievement6UnlockDate(string unlockDate)
    {
        PlayerPrefs.SetString(Constants.achievement6UnlockDate, unlockDate);
    }

    /// <summary>
    /// Get achievement 6 unlock date
    /// </summary>
    /// <returns>Unlock date or null</returns>
    public static string GetAchievement6UnlockDate()
    {
        return PlayerPrefs.GetString(Constants.achievement6UnlockDate, null);
    }
}
