﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Achievement
{
    public int id;
    public string description;
    public bool unlocked;
    public string unlockDate;

    public Achievement(int _id, string _description)
    {
        id = _id;
        description = _description;
        unlocked = AchievementController.CheckIfAchievementUnlocked(id);
        unlockDate = AchievementController.GetAchievementUnlockedDate(id);
    }

    /// <summary>
    /// Unlock achievement, set unlocked to true and unlock date to today, move to unlocked achievements list
    /// </summary>
    public void UnlockAchievement()
    {
        unlocked = true;
        unlockDate = System.DateTime.Now.ToString(Constants.achievementDateFormat);
        GameplayController.instance.UnlockAchievement(this);

        AchievementController.lockedAchievements.Remove(this);
        AchievementController.unlockedAchievements.Add(this);
    }
}
