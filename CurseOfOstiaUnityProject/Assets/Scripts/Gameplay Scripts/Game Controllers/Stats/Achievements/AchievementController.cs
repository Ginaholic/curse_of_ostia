﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class AchievementController : MonoBehaviour
{
    public static AchievementController instance;

    public GameObject achievementList;
    
    public static List<Achievement> unlockedAchievements = new List<Achievement>();
    public static List<Achievement> lockedAchievements = new List<Achievement>();

    void Awake()
    {
        MakeInstance();
    }

    void Start()
    {
        //Add all achievements to UI list with unlocked at the top
        foreach (Achievement achievement in unlockedAchievements)
        {
            CreateUnlockedAchievementGameObject(achievement);
        }
        foreach (Achievement achievement in lockedAchievements)
        {
            CreateLockedAchievementGameObject(achievement);
        }
    }

    /// <summary>
    /// Initialise controller instance
    /// </summary>
    void MakeInstance()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    /// <summary>
    /// Create an achievement UI game object for a locked achievement
    /// </summary>
    /// <param name="achievement">Locked achievement to display</param>
    public void CreateLockedAchievementGameObject(Achievement achievement)
    {
        GameObject lockedAchievement = Instantiate(Resources.Load(Constants.lockedAchievementPrefab) as GameObject);
        lockedAchievement.transform.SetParent(achievementList.transform);
        lockedAchievement.transform.localScale = new Vector3(1, 1, 1);
        lockedAchievement.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = achievement.description;
    }

    /// <summary>
    /// Create an achievemt UI game object for an unlocked achievement
    /// </summary>
    /// <param name="achievement">Unlocked achievement to display</param>
    public void CreateUnlockedAchievementGameObject(Achievement achievement)
    {
        GameObject unlockedAchievement = Instantiate(Resources.Load(Constants.unlockedAchievementPrefab) as GameObject);
        unlockedAchievement.transform.SetParent(achievementList.transform);
        unlockedAchievement.transform.localScale = new Vector3(1, 1, 1);
        unlockedAchievement.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = achievement.description;
        unlockedAchievement.transform.GetChild(1).GetChild(0).GetComponent<TextMeshProUGUI>().text = achievement.unlockDate;
    }

    /// <summary>
    /// Check if achievement is unlocked by searching for an unlock date
    /// </summary>
    /// <param name="achievementId">Id of the achievement to check</param>
    /// <returns>Bool achievement unlocked</returns>
    public static bool CheckIfAchievementUnlocked(int achievementId)
    {
        switch (achievementId)
        {
            case Constants.unlockBowUpgradesAchievementIndex:
                return StatsTracker.GetAchievement1UnlockDate() == Constants.empty;

            case Constants.unlockArrowUpgradesAchievementIndex:
                return StatsTracker.GetAchievement2UnlockDate() == Constants.empty;

            case Constants.purchaseBowUpgradesAchievementIndex:
                return StatsTracker.GetAchievement3UnlockDate() == Constants.empty;

            case Constants.purchaseArrowUpgradesAchievementIndex:
                return StatsTracker.GetAchievement4UnlockDate() == Constants.empty;

            case Constants.secretPathsAchievementIndex:
                return StatsTracker.GetAchievement5UnlockDate() == Constants.empty;

            case Constants.emelynsItemsAchievementIndex:
                return StatsTracker.GetAchievement6UnlockDate() == Constants.empty;
        }
        return false;
    }

    /// <summary>
    /// Get achievement unlocked date
    /// </summary>
    /// <param name="achievementId">Id of the achievement to check</param>
    /// <returns>Unlocked date</returns>
    public static string GetAchievementUnlockedDate(int achievementId)
    {
        switch (achievementId)
        {
            case Constants.unlockBowUpgradesAchievementIndex:
                return StatsTracker.GetAchievement1UnlockDate();

            case Constants.unlockArrowUpgradesAchievementIndex:
                return StatsTracker.GetAchievement2UnlockDate();

            case Constants.purchaseBowUpgradesAchievementIndex:
                return StatsTracker.GetAchievement3UnlockDate();

            case Constants.purchaseArrowUpgradesAchievementIndex:
                return StatsTracker.GetAchievement4UnlockDate();

            case Constants.secretPathsAchievementIndex:
                return StatsTracker.GetAchievement5UnlockDate();

            case Constants.emelynsItemsAchievementIndex:
                return StatsTracker.GetAchievement6UnlockDate();
        }
        return null;
    }

    /// <summary>
    /// Get achievement from list
    /// </summary>
    /// <param name="achievementId">Id of the achievement to get</param>
    /// <returns>Achievement</returns>
    public static Achievement GetAchievement(int achievementId)
    {
        Achievement result1 = unlockedAchievements.Find(x => x.id == achievementId);
        Achievement result2 = lockedAchievements.Find(x => x.id == achievementId);

        if (result1 != null)
        {
            return result1;
        }
        else if (result2 != null)
        {
            return result2;
        }
        return null;
    }

    /// <summary>
    /// Initialise all achievements and add to appropriate list
    /// </summary>
    public static void InitialiseAchievements()
    {
        List<Achievement> unsortedAchievements = new List<Achievement>();

        unsortedAchievements.Add(new Achievement(Constants.unlockBowUpgradesAchievementIndex, Constants.unlockBowUpgradesAchievementDescription));
        unsortedAchievements.Add(new Achievement(Constants.unlockArrowUpgradesAchievementIndex, Constants.unlockArrowUpgradesAchievementDescription));
        unsortedAchievements.Add(new Achievement(Constants.purchaseBowUpgradesAchievementIndex, Constants.purchaseBowUpgradesAchievementDescription));
        unsortedAchievements.Add(new Achievement(Constants.purchaseArrowUpgradesAchievementIndex, Constants.purchaseArrowUpgradesAchievementDescription));
        unsortedAchievements.Add(new Achievement(Constants.secretPathsAchievementIndex, Constants.secretPathsAchievementDescription));
        unsortedAchievements.Add(new Achievement(Constants.emelynsItemsAchievementIndex, Constants.emelynsItemsAchievementDescription));

        foreach (Achievement achievement in unsortedAchievements)
        {
            if (CheckIfAchievementUnlocked(achievement.id))
            {
                unlockedAchievements.Add(achievement);
            }
            else
            {
                lockedAchievements.Add(achievement);
            }
        }
    }
}