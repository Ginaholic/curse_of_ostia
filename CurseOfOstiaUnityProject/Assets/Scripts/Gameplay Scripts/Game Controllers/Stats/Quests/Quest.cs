﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quest
{
    public int id;
    public string description;
    public QuestProgress progress;
    public string reward;
    public bool active;
    public bool completed;

    public Quest(int _id, string _description, int _progressRequiredCount, string _reward)
    {
        id = _id;
        description = _description;
        progress = _progressRequiredCount > 0 ? new QuestProgress(0, _progressRequiredCount) : null;
        reward = _reward;
        active = false;
        completed = false;
    }

    /// <summary>
    /// Complete quest, set active to false and completed to true, move to inactive quest list, gain quest reward
    /// </summary>
    public void CompleteQuest()
    {
        active = false;
        completed = true;
        GameplayController.instance.CompleteQuest(this);
                
        QuestController.activeQuests.Remove(this);
        QuestController.inactiveQuests.Add(this);
        QuestController.instance.RefreshQuestList();

        UpgradesController.instance.GainCurrency(reward);
    }

    /// <summary>
    /// Activate quest, set active to true and completed to false, move to active quest list
    /// </summary>
    public void ActivateQuest()
    {
        active = true;
        completed = false;

        if (GameplayController.instance != null)
        {
            GameplayController.instance.NewQuest(this);
        }
        
        QuestController.inactiveQuests.Remove(this);
        QuestController.activeQuests.Add(this);
        QuestController.instance.RefreshQuestList();
    }

    /// <summary>
    /// Update quest progress, complete if requirement met
    /// </summary>
    public void UpdateQuestProgess()
    {
        if (progress.currentCount + 1 == progress.requiredCount)
        {
            CompleteQuest();
        }
        else
        {
            QuestProgress newProgress = new QuestProgress(progress.currentCount + 1, progress.requiredCount);
            progress = newProgress;
            QuestController.instance.RefreshQuestList();
        }
    }
}

public class QuestProgress  {

    public int currentCount;
    public int requiredCount;
    public string progressText;

    public QuestProgress(int _currentCount, int _requiredCount)
    {
        currentCount = _currentCount;
        requiredCount = _requiredCount;
        progressText = _currentCount.ToString() + Constants.questProgressSeperator + _requiredCount.ToString();
    }
}
