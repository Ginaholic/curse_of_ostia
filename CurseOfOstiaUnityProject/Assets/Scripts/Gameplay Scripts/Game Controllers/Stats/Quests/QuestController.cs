﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestController : MonoBehaviour
{
    public static QuestController instance;

    public GameObject questList;
    public GameObject questsHidden;

    public static List<Quest> inactiveQuests = new List<Quest>();
    public static List<Quest> activeQuests = new List<Quest>();

    void Awake()
    {
        MakeInstance();
    }

    void Start()
    {
        InitialiseQuests();
    }

    /// <summary>
    /// Initialise controller instance
    /// </summary>
    void MakeInstance()
    {
        if (instance == null)
        {
            instance = this;
        }
    }
    
    /// <summary>
    /// Refresh quest list UI, destroy all and repopulate, hide if no active quests
    /// </summary>
    public void RefreshQuestList()
    {
        if (activeQuests.Count == 0)
        {
            questList.SetActive(false);
            questsHidden.SetActive(true);
        }
        else
        {
            if (questList.transform.childCount > 1)
            {
                for (int i = 1; i < questList.transform.childCount; i++)
                {
                    Destroy(questList.transform.GetChild(i).gameObject);
                }
            }
            
            foreach (Quest quest in activeQuests)
            {
                CreateQuestGameOject(quest);
            }
        }
    }

    /// <summary>
    /// Create a quest UI game object
    /// </summary>
    /// <param name="quest">Quest to display</param>
    public void CreateQuestGameOject(Quest quest)
    {
        GameObject activeQuest = Instantiate(Resources.Load(Constants.activeQuestInfoPrefab) as GameObject);
        activeQuest.name = quest.description;
        activeQuest.transform.SetParent(questList.transform);
        activeQuest.transform.localScale = new Vector3(1, 1, 1);
        activeQuest.transform.GetChild(1).GetComponent<Text>().text = quest.description;
        if (quest.progress != null)
        {
            activeQuest.transform.GetChild(2).GetComponent<Text>().text = quest.progress.progressText;
        }
        else
        {
            activeQuest.transform.GetChild(2).GetComponent<Text>().text = Constants.empty;
        }
    }
    
    /// <summary>
    /// Check if quest is active
    /// </summary>
    /// <param name="questId">Id of quest to check</param>
    /// <returns>Bool quest active</returns>
    public static bool CheckIfQuestActive(int questId)
    {
        Quest result = activeQuests.Find(x => x.id == questId);
        return result != null && result.active;
    }

    /// <summary>
    /// Check if quest complete
    /// </summary>
    /// <param name="questId">Id of quest to check</param>
    /// <returns>Bool quest complete</returns>
    public static bool CheckIfQuestComplete(int questId)
    {
        Quest result = inactiveQuests.Find(x => x.id == questId);
        return result != null && result.completed;
    }

    /// <summary>
    /// Get quest from list
    /// </summary>
    /// <param name="questId">Id of quest to get</param>
    /// <returns>Quest</returns>
    public static Quest GetQuest(int questId)
    {
        Quest result1 = inactiveQuests.Find(x => x.id == questId);
        Quest result2 = activeQuests.Find(x => x.id == questId);

        if (result1 != null)
        {
            return result1;
        }
        else if (result2 != null)
        {
            return result2;
        }
        return null;
    }

    /// <summary>
    /// Initialise all quests, add to inactive list. Activate first (main) quest
    /// </summary>
    public static void InitialiseQuests()
    {
        inactiveQuests.Clear();
        activeQuests.Clear();
        
        inactiveQuests.Add(new Quest(Constants.findEmelynQuestIndex, Constants.findEmelynQuestDescription, Constants.noQuestProgressCount, Constants.purpleGemReward));
        inactiveQuests.Add(new Quest(Constants.killMolemenQuestIndex, Constants.killMolemenQuestDescription, Constants.killQuestProgressCount, Constants.silverGemReward));
        inactiveQuests.Add(new Quest(Constants.killTreantsQuestIndex, Constants.killTreantsQuestDescription, Constants.killQuestProgressCount, Constants.silverGemReward));
        inactiveQuests.Add(new Quest(Constants.upgradeBowQuestIndex, Constants.upgradeBowQuestDescription, Constants.noQuestProgressCount, Constants.blueGemReward));
        inactiveQuests.Add(new Quest(Constants.upgradeArrowQuestIndex, Constants.upgradeArrowQuestDescription, Constants.noQuestProgressCount, Constants.blueGemReward));
        
        GetQuest(Constants.findEmelynQuestIndex).ActivateQuest();
    }
}
