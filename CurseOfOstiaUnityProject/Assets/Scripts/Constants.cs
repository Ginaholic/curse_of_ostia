﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Constants
{
    //Tag names
    public static string playerTag = "Player";
    public static string enemyTag = "Enemy NPC";
    public static string arrowTag = "Arrow";
    public static string bombTag = "Bomb";
    public static string pickUpItemTag = "Pick Up Item";
    public static string environmentObjectTag = "Environment Object";
    public static string waterAreaTag = "Water Area";
    public static string instantDeathAreaTag = "Instant Death";

    //Player animator parameters and states
    public static string animatorFaceXFloat = "FaceX";
    public static string animatorFaceYFloat = "FaceY";
    public static string animatorIdleState = "Idle";
    public static string animatorWalkState = "Walk";

    public static string playerShootState = "Shoot";
    public static string playerPunchState = "Punch";
    
    public static string enemyDieState = "Die";

    //Message animator states
    public static string messageSlideInState = "SlideIn";
    public static string messageSlideOutState = "SlideOut";

    //Scene animator states
    public static string sceneFadeInState = "FadeIn";
    public static string sceneFadeOutState = "FadeOut";

    //Prefab names
    public static string fullHeartPrefabPath = "UI/Full Heart";
    public static string halfHeartPrefabPath = "UI/Half Heart";
    public static string emptyHeartPrefabPath = "UI/Empty Heart";
    public static string activeQuestInfoPrefab = "UI/Quest";
    public static string lockedAchievementPrefab = "UI/Locked Achievement";
    public static string unlockedAchievementPrefab = "UI/Unlocked Achievement";
    public static string nameCloneSuffix = "(Clone)";
    public static string empty = "";

    //Prefab folder names
    public static string weaponsPrefabFolder = "Weapons/";
    public static string itemsPrefabFolder = "Items/";
    public static string enemiesPrefabFolder = "Enemies/";

    //Item names
    public static string bombItemName = "Bomb";
    public static string appleItemName = "Apple";
    public static string cherryItemName = "Cherry";
    public static string healthPotionItemName = "Health Potion";
    public static string staminaPotionItemName = "Stamina Potion";
    public static string purpleGemItemName = "Purple Gem";
    public static string blueGemItemName = "Blue Gem";
    public static string silverGemItemName = "Silver Gem";
    public static string bowBook1ItemName = "Bow Book 1";
    public static string bowBook2ItemName = "Bow Book 2";
    public static string arrowBook1ItemName = "Arrow Book 1";
    public static string arrowBook2ItemName = "Arrow Book 2";

    public static string emelynNecklaceItemName = "Emelyn's Necklace";
    public static string emelynStaffItemName = "Emelyn's Staff";
    public static string emelynScrollItemName = "Emelyn's Scroll";
    public static string emelynBookItemName = "Emelyn's Spell Book";

    //Item descriptions
    public static string bombItemDescription = "This could do some damage if you throw it!";
    public static string appleItemDescription = "Increased Power";
    public static string cherryItemDescription = "Invisible";
    public static string healthPotionItemDescription = "Gain 1 Life";
    public static string staminaPotionItemDescription = "Increased Speed";
    public static string purpleGemItemDescription = "Purple Gem";
    public static string blueGemItemDescription = "Blue Gem";
    public static string silverGemItemDescription = "Silver Gem";
    public static string bowBook1ItemDescription = "Bow Tier 2 unlocked";
    public static string bowBook2ItemDescription = "Bow Tier 3 unlocked";
    public static string arrowBook1ItemDescription = "Arrow Tier 2 unlocked";
    public static string arrowBook2ItemDescription = "Arrow Tier 3 unlocked";

    public static string emelynNecklaceItemDescription = "Hmm, this looks familiar. You should keep hold of it for a while";
    public static string emelynStaffItemDescription = "Is this Emelyn's staff? She's bound to be in trouble without it!";
    public static string emelynScrollItemDescription = "This is a scroll from the Academy... Emelyn must have dropped this here";
    public static string emelynBookItemDescription = "Perhaps you should return this to Emelyn";

    //Quest rewards
    public static string purpleGemReward = "1 Purple Gem";
    public static string blueGemReward = "1 Blue Gem";
    public static string silverGemReward = "1 Silver Gem";
    public static string gemRewardModifier = "1 ";

    //Message titles
    public static string itemLimitTitle = "Item limit";
    public static string bagLimitTitle = "Bag limit";
    public static string upgradeLockedTitle = "Upgrade Locked";
    public static string purchaseUnavailableTitle = "Purchase Unavailable";
    public static string mysteriousFruitsTitle = "Mysterious Fruits";
    public static string achivementUnlockedTitle = "Achievement Unlocked!";
    public static string questCompletedTitle = "Quest Completed!";
    public static string questUnlockedTitle = "New Quest Unlocked!";

    //Message texts
    public static string itemLimitMessage = "You can't carry any more of those";
    public static string bagLimitMessage = "You can't carry any more";
    public static string upgradeLockedMessage = "You haven't learnt this yet";
    public static string purchaseUnavailableMessage = "You don't have enough gems to buy this yet";
    public static string mysteriousFruitsMessage = "The fruits here look strange. You should try one!";

    //Arrow directions
    public static string arrowUp = "Arrow Up";
    public static string arrowDown = "Arrow Down";
    public static string arrowLeft = "Arrow Left";
    public static string arrowRight = "Arrow Right";

    //Punch directions
    public static string punchUp = "Punch Up";
    public static string punchDown = "Punch Down";
    public static string punchLeft = "Punch Left";
    public static string punchRight = "Punch Right";

    //Enemy attack directions
    public static string enemyAttackUp = "Enemy Attack Up";
    public static string enemyAttackDown = "Enemy Attack Down";
    public static string enemyAttackLeft = "Enemy Attack Left";
    public static string enemyAttackRight = "Enemy Attack Right";

    //Player directions
    public static string playerUp = "Up";
    public static string playerDown = "Down";
    public static string playerLeft = "Left";
    public static string playerRight = "Right";

    //Input axes
    public static string horizontalInput = "Horizontal";
    public static string verticalInput = "Vertical";
    public static string primaryAttackInput = "Primary Attack";
    public static string secondaryAttackInput = "Secondary Attack";

    //Options
    public static string soundOn = "SoundsOn";
    public static string sfxVolume = "SFXVolume";
    public static string musicVolume = "MusicVolume";

    //Scene names
    public static string mainMenuScene = "Main Menu";
    public static string optionsScene = "Options";
    public static string achievementsScene = "Achievements";
    public static string levelScene = "Level";

    //Upgrade tiers
    public static string arrowUpgradeTier = "Arrow";
    public static string bowUpgradeTier = "Bow";

    //Enemy types
    public static string enemyTypeMoleman = "Moleman";
    public static string enemyTypeTreant = "Treant";

    //Enemy agent states
    public static string enemyAgentPatrolState = "Patrolling";
    public static string enemyAgentChaseState = "Chasing";
    public static string enemyAgentAttackState = "Attacking";

    //Achievement descriptions
    public static string unlockBowUpgradesAchievementDescription = "Unlock all bow upgrades";
    public static string unlockArrowUpgradesAchievementDescription = "Unlock all arrow upgrades";
    public static string purchaseBowUpgradesAchievementDescription = "Purchase all bow upgrades";
    public static string purchaseArrowUpgradesAchievementDescription = "Purchase all arrow upgrades";
    public static string secretPathsAchievementDescription = "Discover all secret paths";
    public static string emelynsItemsAchievementDescription = "Recover all of Emelyn's items";

    //Quest descriptions
    public static string findEmelynQuestDescription = "Find Emelyn";
    public static string killMolemenQuestDescription = "Kill 5 Molemen";
    public static string killTreantsQuestDescription = "Kill 5 Treants";
    public static string upgradeBowQuestDescription = "Upgrade Bow";
    public static string upgradeArrowQuestDescription = "Upgrade Arrow";

    //Stat recording names
    public static string emelynFound = "EmelynFound";
    public static string bowUpgraded = "BowUpgraded";
    public static string arrowUpgraded = "ArrowUpgraded";
    public static string molemenKillCount = "MolemenKillCount";
    public static string treantKillCount = "TreantKillCount";
    public static string emelynItemCount = "EmelynItemCount";
    public static string secretPathFoundCount = "SecretPathFoundCount";
    public static string bowUpgradePurchasedCount = "BowUpgradePurchasedCount";
    public static string arrowUpgradePurchasedCount = "ArrowUpgradePurchasedCount";
    public static string bowUpgradeUnlockedCount = "BowUpgradeUnlockedCount";
    public static string arrowUpgradeUnlockedCount = "ArrowUpgradeUnlockedCount";
    public static string achievement1UnlockDate = "Achievement1UnlockDate";
    public static string achievement2UnlockDate = "Achievement2UnlockDate";
    public static string achievement3UnlockDate = "Achievement3UnlockDate";
    public static string achievement4UnlockDate = "Achievement4UnlockDate";
    public static string achievement5UnlockDate = "Achievement5UnlockDate";
    public static string achievement6UnlockDate = "Achievement6UnlockDate";

    //Other
    public static string gameInitialised = "Game Initialised";
    public static string achievementDateFormat = "dd/MM/yyyy";
    public static string questProgressSeperator = "/";

    public const int collisionNone = 1;
    public const int collisionTop = 1;
    public const int collisionBottom = 2;
    public const int collisionLeft = 3;
    public const int collisionRight = 4;
    
    //Max constants
    public static int arrowMaxSpeed = 10;
    public static int punchDisplayTime = 1;
    public static int enemyDeadDisplayTime = 1;
    public static int enemyAttackDisplayTime = 1;

    public static float sceneFadeInDisplayTime = 0.5f;
    public static float sceneFadeOutDisplayTime = 0.5f;

    public static float playerMaxLives = 3;
    public static float playerSpeed = 100;
    public static int playerMaxMeleeDamage = 10;
    public static float playerMaxMeleeRange = 1.5f;

    public static float enemyMaxHealth = 100;
    public static float enemySpeed = 60;
    public static float enemyAttackDamage = 0.5f;
    public static float enemyAttackRange = 1;
    public static float enemyVisionRange = 4;
    public static float enemyVisionMinAngle = -90;
    public static float enemyVisionMaxAngle = 90;
    public static float enemyAttackTimerMin = 2.5f;

    public static float windOnTime = 10f;
    public static float windOffTime = 30f;

    public static int sortingOrderBase = 100;

    public static int defaultMaxItemCount = 3;
    public static int emylynItemMaxItemCount = 1;
    public static int emylynItemCount = 4;

    public static int secretPathsCount = 4;

    public static int bowUpgradesCount = 2;
    public static int arrowUpgradesCount = 2;

    public static int defaultNewSlotCount = 1;
    public static int defaultEmptySlotCount = 0;

    public static float defaultFrictionForce = 0.4f;

    //Audio clip indexes
    public static int openInventoryClip = 0;
    public static int openUpgradesClip = 1;
    public static int pickUpClip = 2;
    public static int purchaseClip = 3;
    public static int loseLifeClip = 4;
    public static int explosionClip = 5;
    public static int achievementClip = 6;
    public static int secretPathClip = 7;

    //Message type indexes
    public static int shortMessage = 1;
    public static int longMessage = 2;
    public static int questMessage = 3;

    //Reward sprite indexes
    public static int silverGemRewardSprite = 0;
    public static int blueGemRewardSprite = 1;
    public static int purpleGemRewardSprite = 2;
    
    //Achievement indexes
    public const int unlockBowUpgradesAchievementIndex = 1;
    public const int unlockArrowUpgradesAchievementIndex = 2;
    public const int purchaseBowUpgradesAchievementIndex = 3;
    public const int purchaseArrowUpgradesAchievementIndex = 4;
    public const int secretPathsAchievementIndex = 5;
    public const int emelynsItemsAchievementIndex = 6;

    //Upgrade tier indexes
    public static int upgradeTier0 = 0;
    public static int upgradeTier1 = 1;
    public static int upgradeTier2 = 2;
    
    //Quest indexes
    public static int findEmelynQuestIndex = 1;
    public static int killMolemenQuestIndex = 2;
    public static int killTreantsQuestIndex = 3;
    public static int upgradeBowQuestIndex = 4;
    public static int upgradeArrowQuestIndex = 5;

    //Quest progress counts
    public static int noQuestProgressCount = 0;
    public static int killQuestProgressCount = 5;

    //Wind direction indexes
    public const int windRighttIndex = 0;
    public const int windUpIndex = 1;
    public const int windLeftIndex = 2;
    public const int windDownIndex = 3;

    //Wind direction forces
    public static Vector2 windRightForce = new Vector2(5, 0);
    public static Vector2 windUpForce = new Vector2(0, 5);
    public static Vector2 windLeftForce = new Vector2(-5, 0);
    public static Vector2 windDownForce = new Vector2(0, -5);

    //Tilemap node constants
    public static int tilemapAreaLeft = -40;
    public static int tilemapAreaBottom = -10;
    public static int tilemapAreaRight = 35;
    public static int tilemapAreaTop = 37;

    public static float distanceToNodeCenter = 0.5f;
    public static int distanceBetweenNodes = 1;
    public static float distanceToPoint = 0.5f;
}
