# Curse of Ostia

The Curse of Ostia is a 2D top-down adventure game created using Unity 2018, displaying a custom physics and AI engine implementing AABB collision detection and A* pathfinding via a point graph.
[Available for download from Itch.io](https://scorepio-games.itch.io/curse-of-ostia).


## Installation

Clone the repo

```
git clone git@gitlab.com:Ginaholic/curse_of_ostia.git
```

Open the project with Unity and hit the play button.

Thanks

## Controls

WASD or Arrow Keys to move
Left click to shoot an arrow
Right click to punch

## Debug Options

In the pause menu, found by clicking the settings cog in the upper right-hand corner, there is a "Debug" option.
Clicking this will take you to a panel where debug graphics for collision boundaries, paths and nodes can be turned on or off individually.

## Licenses and Credit

* "Bitty Font" by Mash Arcade licensed CC0: https://masharcade.itch.io/bitty
* "Pixel Art Particles Pack" by Zakhan licensed Unity-EULA: https://assetstore.unity.com/packages/vfx/particles/pixel-art-particles-pack-129939
* "Pixel UI" by Pixelsoft Games licensed Unity-EULA: https://assetstore.unity.com/packages/2d/gui/pixel-ui-128440
* "Generic RPG Pixel Pack" by Estudio Vaca Roxa licensed CCO: https://bakudas.itch.io/generic-rpg-pack
* "PMSFX" and "PM:SAMPLER" by Phil Michalski licensed AudioPhil-EULA:https://www.pmsfx.com/free/
* "RPG Audio" by Kenney Game Assets licensed CC0: https://kenney.nl/assets/rpg-audio
* "Kyrise's Free 16x16 RPG Icon Pack" by Kyrise licensed CC4: https://kyrise.itch.io/kyrises-free-16x16-rpg-icon-pack"
* "RPG-Items" by Jesse (GentleCatStudio) licensed CC0: https://gentlecatstudio.itch.io/rpg-items
* "TinyRPG Forest" and "TinyRPG Dungeon" by Ansimuz licensed CC0: https://itch.io/c/193687/all-assets-packs
* "Health Bar Assest Pack" by Adwit Rahman licensed CC0: https://adwitr.itch.io/pixel-health-bar-asset-pack


## Missing Features

1. There is currently no implementation for the bomb attacks.
1. Pick up items have no effect on the player besides the health potion.
1. All enemies currently have the same behaviour attributes.

